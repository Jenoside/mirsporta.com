<?php

class siteViewHelper extends waAppViewHelper
{
    public function pages($parent_id = 0, $with_params = true)
    {
        if (is_bool($parent_id)) {
            $with_params = $parent_id;
            $parent_id = 0;
        }
        try {
            $domain_model = new siteDomainModel();
            $domain = $domain_model->getByName(waSystem::getInstance()->getRouting()->getDomain(null, true));

            $page_model = new sitePageModel();
            $sql = "SELECT id, parent_id, name, title, full_url, url, create_datetime, update_datetime FROM ".$page_model->getTableName().'
                    WHERE domain_id = i:domain_id AND route = s:route AND status = 1 ORDER BY sort';

            if (wa()->getApp() == 'site') {
                $route = wa()->getRouting()->getRoute('url');
                $url = $this->wa()->getAppUrl(null, true);
            } else {
                $routes = wa()->getRouting()->getByApp('site', $domain['name']);
                if ($routes) {
                    $route = current($routes);
                    $route = $route['url'];
                    $url = wa()->getRootUrl(false, true).waRouting::clearUrl($route);
                } else {
                    return array();
                }
            }

            $pages = $page_model->query($sql, array(
                'domain_id' => $domain['id'],
                'route' => $route)
            )->fetchAll('id');

            if ($with_params) {
                $page_params_model = new sitePageParamsModel();
                $data = $page_params_model->getByField('page_id', array_keys($pages), true);
                foreach ($data as $row) {
                    $pages[$row['page_id']][$row['name']] = $row['value'];
                }
            }
            foreach ($pages as &$page) {
                $page['url'] = $url.$page['full_url'];
                if (!isset($page['title']) || !$page['title']) {
                    $page['title'] = $page['name'];
                }
                foreach ($page as $k => $v) {
                    if ($k != 'content') {
                        $page[$k] = htmlspecialchars($v);
                    }
                }
            }
            unset($page);
            // make tree
            foreach ($pages as $page_id => $page) {
                if ($page['parent_id'] && isset($pages[$page['parent_id']])) {
                    $pages[$page['parent_id']]['childs'][] = &$pages[$page_id];
                }
            }
            if ($parent_id) {
                return isset($pages[$parent_id]['childs']) ? $pages[$parent_id]['childs'] : array();
            }
            foreach ($pages as $page_id => $page) {
                if ($page['parent_id'] && $page_id != $parent_id) {
                    unset($pages[$page_id]);
                }
            }
            return $pages;
        } catch (Exception $e) {
            return array();
        }
    }

    public function page($id)
    {
        $page_model = new sitePageModel();
        $page = $page_model->getById($id);
        $page['content'] = $this->wa()->getView()->fetch('string:'.$page['content']);

        $page_params_model = new sitePageParamsModel();
        $page += $page_params_model->getById($id);

        return $page;
    }

    public function getCity(){
      $curCity = '';
      $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
      $actual_link = str_replace('https://','',$actual_link);
      $curSub = explode('.', $actual_link)[0];
      switch ($curSub) {
        case 'abakan':
          $curCity = 'Абакан';
          break;
        case 'aksai':
          $curCity = 'Аксай';
          break;
        case 'almetevsk':
          $curCity = 'Альметьевск';
          break;
        case 'arhangelsk':
          $curCity = 'Архангельск';
          break;
        case 'armavir':
          $curCity = 'Армавир';
          break;
        case 'artem':
          $curCity = 'Артем';
          break;
        case 'astrahan':
          $curCity = 'Астрахань';
          break;
        case 'balakovo':
          $curCity = 'Балаково';
          break;
        case 'barnaul':
          $curCity = 'Барнаул';
          break;
        case 'berezniki':
          $curCity = 'Березники';
          break;
        case 'bisk':
          $curCity = 'Бийск';
          break;
        case 'blagoveshensk':
          $curCity = 'Благовещенск';
          break;
        case 'bor':
          $curCity = 'Бор';
          break;
        case 'borisoglebsk':
          $curCity = 'Борисоглебск';
          break;
        case 'bratsk':
          $curCity = 'Брацк';
          break;
        case 'bryansk':
          $curCity = 'Брянск';
          break;
        case 'byddenovsk':
          $curCity = 'Буденновск';
          break;
        case 'cherepovets':
          $curCity = 'Череповец';
          break;
        case 'chita':
          $curCity = 'Чита';
          break;
        case 'dmitrovgrad':
          $curCity = 'Димитровград';
          break;
        case 'dzerzhinsk':
          $curCity = 'Дзержинск';
          break;
        case 'ekatrinburg':
          $curCity = 'Екатеринбург';
          break;
        case 'engels':
          $curCity = 'Энгельс';
          break;
        case 'habarovsk':
          $curCity = 'Хабаровск';
          break;
        case 'ioshkar-ola':
          $curCity = 'Йошкар-Ола';
          break;
        case 'irkutsk':
          $curCity = 'Иркутск';
          break;
        case 'ivanovo':
          $curCity = 'Иваново';
          break;
        case 'izhevsk':
          $curCity = 'Ижевск';
          break;
        case 'kaliningrad':
          $curCity = 'Калининград';
          break;
        case 'kaluga':
          $curCity = 'Калуга';
          break;
        case 'kamensk-shahtinskii':
          $curCity = 'Каменск-Шахтинский';
          break;
        case 'kazan':
          $curCity = 'Казань';
          break;
        case 'kineshma':
          $curCity = 'Кинешма';
          break;
        case 'kirov':
          $curCity = 'Киров';
          break;
        case 'komsomolsk-na-amyre':
          $curCity = 'Комсомольск-на-Амуре';
          break;
        case 'kostroma':
          $curCity = 'Кострома';
          break;
        case 'krasnodar':
          $curCity = 'Краснодар';
          break;
        case 'krasnoyarsk':
          $curCity = 'Красноярск';
          break;
        case 'kyrsk':
          $curCity = 'Курск';
          break;
        case 'kyznetsk':
          $curCity = 'Кузнецк';
          break;
        case 'lipetsk':
          $curCity = 'Липецк';
          break;
        case 'magadan':
          $curCity = 'Магадан';
          break;
        case 'magnitogorsk':
          $curCity = 'Магнитогорск';
          break;
        case 'miass':
          $curCity = 'Миасс';
          break;
        case 'myrmansk':
          $curCity = 'Мурманск';
          break;
        case 'nabereznie-chelni':
          $curCity = 'Набережные Челны';
          break;
        case 'nalchik':
          $curCity = 'Нальчик';
          break;
        case 'neftekamsk':
          $curCity = 'Нефтекамск';
          break;
        case 'nevinomissk':
          $curCity = 'Невинномысск';
          break;
        case 'nizhnevartovsk':
          $curCity = 'Нижневартовск';
          break;
        case 'nizhnii-novgorod':
          $curCity = 'Нижний Новгород';
          break;
        case 'nizhnii-tagil':
          $curCity = 'Нижний Тагил';
          break;
        case 'novochebksarsk':
          $curCity = 'Новочебоксарск';
          break;
        case 'novocherkassk':
          $curCity = 'Новочеркасск';
          break;
        case 'novokyznetsk':
          $curCity = 'novokyznetsk';
          break;
        case 'novorosiisk':
          $curCity = 'Новороссийск';
          break;
        case 'novosibirsk':
          $curCity = 'Новосибирск';
          break;
        case 'noyabrsk':
          $curCity = 'Ноябрьск';
          break;
        case 'oktyabrskii':
          $curCity = 'Октябрьский';
          break;
        case 'omsk':
          $curCity = 'Омск';
          break;
        case 'orel':
          $curCity = 'Орел';
          break;
        case 'orenbyrg':
          $curCity = 'Оренбург';
          break;
        case 'orsk':
          $curCity = 'Орск';
          break;
        case 'penza':
          $curCity = 'Пенза';
          break;
        case 'perm':
          $curCity = 'Перьм';
          break;
        case 'petropavlovsk-kamchatskii':
          $curCity = 'Петропавловск-Камчатский';
          break;
        case 'petrozavodsk':
          $curCity = 'Петрозаводск';
          break;
        case 'podolsk':
          $curCity = 'Подольск';
          break;
        case 'prokopievsk':
          $curCity = 'Прокопьевск';
          break;
        case 'pskov':
          $curCity = 'Псков';
          break;
        case 'pyatigorsk':
          $curCity = 'Пятигорск';
          break;
        case 'ribinsk':
          $curCity = 'Рыбинск';
          break;
        case 'rossosh':
          $curCity = 'Россошь';
          break;
        case 'rostov-na-donu':
          $curCity = 'Ростов на Дону';
          break;
        case 'salavat':
          $curCity = 'Салават';
          break;
        case 'saratov':
          $curCity = 'Саратов';
          break;
        case 'sevastopol':
          $curCity = 'Севастополь';
          break;
        case 'severodvinsk':
          $curCity = 'Северодвинск';
          break;
        case 'shahti':
          $curCity = 'Шахты';
          break;
        case 'siktivkar':
          $curCity = 'Сыктывкар';
          break;
        case 'simferopol':
          $curCity = 'Симферополь';
          break;
        case 'sizran':
          $curCity = 'Сызрань';
          break;
        case 'smolensk':
          $curCity = 'Смоленск';
          break;
        case 'sochi':
          $curCity = 'Сочи';
          break;
        case 'spb':
          $curCity = 'Санкт-Петербург';
          break;
        case 'starii-oskol':
          $curCity = 'Старый Оскол';
          break;
        case 'stavropol':
          $curCity = 'Ставрополь';
          break;
        case 'sterlitamak':
          $curCity = 'Стерлитамак';
          break;
        case 'syrgyt':
          $curCity = 'Сургут';
          break;
        case 'taganrog':
          $curCity = 'Таганрог';
          break;
        case 'tambov':
          $curCity = 'Тамбов';
          break;
        case 'tolyati':
          $curCity = 'Тольятти';
          break;
        case 'tomsk':
          $curCity = 'Томск';
          break;
        case 'tumen':
          $curCity = 'Тюмень';
          break;
        case 'tver':
          $curCity = 'Тверь';
          break;
        case 'tyapse':
          $curCity = 'Туапсе';
          break;
        case 'tyla':
          $curCity = 'Тула';
          break;
        case 'uzhno-shalinsk':
          $curCity = 'Ююжно-Сахалинск';
          break;
        case 'velikii-novgorod':
          $curCity = 'Великий Новгород';
          break;
        case 'vladimir':
          $curCity = 'Владимир';
          break;
        case 'vladivostok':
          $curCity = 'Владивосток';
          break;
        case 'volgograd':
          $curCity = 'Волгоград';
          break;
        case 'vologda':
          $curCity = 'Вологда';
          break;
        case 'volzskii':
          $curCity = 'Волжский';
          break;
        case 'voronezh':
          $curCity = 'Воронеж';
          break;
        case 'yalta':
          $curCity = 'Ялта';
          break;
        case 'yaroslavl':
          $curCity = 'Ярославль';
          break;
        case 'yfa':
          $curCity = 'Уфа';
          break;
        case 'ylan-yde':
          $curCity = 'Улан-Удэ';
          break;
        case 'ylianovsk':
          $curCity = 'Ульяновск';
          break;
        case 'yssyriisk':
          $curCity = 'Уссурийск';
          break;
        case 'zelenodolsk':
          $curCity = 'Зеленодольск';
          break;
        case 'zheleznodorozhnii':
          $curCity = 'Железнодорожный';
          break;
      }
      return $curCity;
    }

    public function getAddress(){
      $curCity = '';
      $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
      $actual_link = str_replace('https://','',$actual_link);
      $curSub = explode('.', $actual_link)[0];
      switch ($curSub) {
        case 'abakan':
          $curCity = 'г. Абакан, Заводская ул., 1В';
          break;
        case 'aksai':
          $curCity = 'г. Аксай, ул. Авиаторов, 5';
          break;
        case 'almetevsk':
          $curCity = 'г. Альметьевск, ул. Объездная, 16
';break;
        case 'arhangelsk':
          $curCity = 'г. Архангельск, ул. Дежнёвцев, 34, стр. 1
';break;
        case 'armavir':
          $curCity = 'г. Армавир, ул. Воровского, 58
';break;
        case 'artem':
          $curCity = 'г. Артём, Солнечная ул., 46
';break;
        case 'astrahan':
          $curCity = 'г. Астрахань, 1-й проезд Рождественского, 1
';break;
        case 'balakovo':
          $curCity = 'г. Балаково, Транспортная ул., 8Б
';break;
        case 'barnaul':
          $curCity = 'г. Барнаул, ул. Чернышевского, 293А';break;
        case 'berezniki':
          $curCity = 'г. Березники, Большевистская ул., 8';break;
        case 'bisk':
          $curCity = 'г. Бийск, ул. имени Героя Советского Союза Васильева, 85
';break;
        case 'blagoveshensk':
          $curCity = 'г. Благовещенск, ул. Театральная, 251
';break;
        case 'bor':
          $curCity = 'г. Бор, Октябрьская ул., 4
';break;
        case 'borisoglebsk':
          $curCity = 'г. Борисоглебск, Матросовская ул., 158
';break;
        case 'bratsk':
          $curCity = 'г. Брацк, Коммунальная ул., 11, стр. 33
';break;
        case 'bryansk':
          $curCity = 'г. Брянск, ул. Бурова, 20
';break;
        case 'byddenovsk':
          $curCity = 'г. Буденновск, Промышленная ул., 2';break;
        case 'cherepovets':
          $curCity = 'г. Череповец, Промышленная ул., 7, стр. 4
';break;
        case 'chita':
          $curCity = 'г. Чита, ул. Сухая Падь, 3
';break;
        case 'dmitrovgrad':
          $curCity = 'г. Димитровград, ул. Ганенкова ул., 55
';break;
        case 'dzerzhinsk':
          $curCity = 'г. Дзержинск, Зарёвская объездная дорога, 9В
';break;
        case 'ekatrinburg':
          $curCity = 'г. Екатеринбург, Норильская ул., 77
';break;
        case 'engels':
          $curCity = 'г. Энгельс, Лесокомбинатская ул., 30
';break;
        case 'habarovsk':
          $curCity = 'г. Хабаровск, Целинная ул., 8
';break;
        case 'ioshkar-ola':
          $curCity = 'г. Йошкар-Ола, Строителей ул., 76а
';break;
        case 'irkutsk':
          $curCity = 'г. Иркутск, ул. Новаторов, 1';break;
        case 'ivanovo':
          $curCity = 'г. Иваново, Сосновая ул., 1, корп. В
';break;
        case 'izhevsk':
          $curCity = 'г. Ижевск, ул. Пойма, д. 22';break;
        case 'kaliningrad':
          $curCity = 'г. Калининград, Московский просп., 184
';break;
        case 'kaluga':
          $curCity = 'г. Калуга, Грабцевское ш., 107
';break;
        case 'kamensk-shahtinskii':
          $curCity = 'г. Каменск-Шахтинский, ул. Героев Пионеров, 16
';break;
        case 'kazan':
          $curCity = 'г. Казань, Аделя Кутуя ул., 151
';break;
        case 'kineshma':
          $curCity = 'г. Кинешма, Вичугская ул., 150
';break;
        case 'kirov':
          $curCity = 'г. Киров, Щорса ул., 105
';break;
        case 'komsomolsk-na-amyre':
          $curCity = 'г. Комсомольск-на-Амуре, Кирова ул., 12/2
';break;
        case 'kostroma':
          $curCity = 'г. Кострома, Зелёная ул., 1А
';break;
        case 'krasnodar':
          $curCity = 'г. Краснодар, ул. Александра Покрышкина, 2/12
';break;
        case 'krasnoyarsk':
          $curCity = 'г. Красноярск, Северное ш., 17
';break;
        case 'kyrsk':
          $curCity = 'г. Курск, ул. 50 лет Октября, 179, вл. 1
';break;
        case 'kyznetsk':
          $curCity = 'г. Кузнецк, Алексеевское ш., 5
';break;
        case 'lipetsk':
          $curCity = 'г. Липецк, Трубный пр., 17Б
';break;
        case 'magadan':
          $curCity = 'г. Магадан, Пролетарская ул., 96
';break;
        case 'magnitogorsk':
          $curCity = 'г. Магнитогорск, 1-я Северо-западная ул., 10
';break;
        case 'miass':
          $curCity = 'г. Миасс, ул. Академика Павлова, 12
';break;
        case 'myrmansk':
          $curCity = 'г. Мурманск, Промышленная ул., 19
';break;
        case 'nabereznie-chelni':
          $curCity = 'г. Набережные Челны, Хлебный проезд, 30
';break;
        case 'nalchik':
          $curCity = 'г. Нальчик, Кузнечный пер., 5
';break;
        case 'neftekamsk':
          $curCity = 'г. Нефтекамск, ул. Высоковольтная, 9о
';break;
        case 'nevinomissk':
          $curCity = 'г. Невинномысск, ул. Пятигорское шоссе, д. 7';break;
        case 'nizhnevartovsk':
          $curCity = 'г. Нижневартовск, ул. Кузоваткина, 5, с.7
';break;
        case 'nizhnii-novgorod':
          $curCity = 'г. Нижний Новгород, Московское ш., 52
';break;
        case 'nizhnii-tagil':
          $curCity = 'г. Нижний Тагил, Восточное ш., 17б
';break;
        case 'novochebksarsk':
          $curCity = 'г. Новочебоксарск, ул. Промышленная, 40 А';break;
        case 'novocherkassk':
          $curCity = 'г. Новочеркасск, Газетная ул., 21
';break;
        case 'novokyznetsk':
          $curCity = 'г. Новокузнецк, Полесская ул., 15
';break;
        case 'novorosiisk':
          $curCity = 'г. Новороссийск, Промышленная ул., 5, село Гайдук';break;
        case 'novosibirsk':
          $curCity = 'г. Новосибирск, Станционная ул., 80, корп. 2
';break;
        case 'noyabrsk':
          $curCity = 'г. Ноябрьск, 3-й проезд, 9В/11
';break;
        case 'oktyabrskii':
          $curCity = 'г. Октябрьский, ул. Северная 25к8
';break;
        case 'omsk':
          $curCity = 'г. Омск, ул. Омская, 221
';break;
        case 'orel':
          $curCity = 'г. Орел, Ливенская ул., 68А
';break;
        case 'orenbyrg':
          $curCity = 'г. Оренбург, Загородное шоссе, 3
';break;
        case 'orsk':
          $curCity = 'г. Орск, Новотроицкое шоссе, 11
';break;
        case 'penza':
          $curCity = 'г. Пенза, Совхозная ул., 15, литера З
';break;
        case 'perm':
          $curCity = 'г. Перьм, Танкистов ул., 50
';break;
        case 'petropavlovsk-kamchatskii':
          $curCity = 'г. Петропавловск-Камчатский, Вулканная ул., 59, корп. 3';break;
        case 'petrozavodsk':
          $curCity = 'г. Петрозаводск, ул. Коммунистов, 50
';break;
        case 'podolsk':
          $curCity = 'г. Подольск, ул. Лобачёва, 14
';break;
        case 'prokopievsk':
          $curCity = 'г. Прокопьевск, ул. Гайдара, 45
';break;
        case 'pskov':
          $curCity = 'г. Псков, ул. Леона Поземского, 110Д, Псков (пом.1001)';break;
        case 'pyatigorsk':
          $curCity = 'г. Пятигорск, ул. Егоршина, 6, стр. 1
';break;
        case 'ribinsk':
          $curCity = 'г. Рыбинск, Ярославский тракт, 52
';break;
        case 'rossosh':
          $curCity = 'г. Россошь, ул. Мира, 201
';break;
        case 'rostov-na-donu':
          $curCity = 'г. Ростов-На-Дону, Каширская ул., 5
';break;
        case 'salavat':
          $curCity = 'г. Салават, ул. Нуриманова, 30
';break;
        case 'saratov':
          $curCity = 'г. Саратов, ВСО п. Строитель, склады "Логистик-центра"
';break;
        case 'sevastopol':
          $curCity = 'г. Севастополь, Фиолентовское ш., 1/5
';break;
        case 'severodvinsk':
          $curCity = 'г. Северодвинск, Беломорский просп., 3
';break;
        case 'shahti':
          $curCity = 'г. Шахты, Газетный пер., 4Б
';break;
        case 'siktivkar':
          $curCity = 'г. Сыктывкар, Сысольское ш., 33
';break;
        case 'simferopol':
          $curCity = 'г. Симферополь, ул. Глинки, 67Г
';break;
        case 'sizran':
          $curCity = 'г. Сызрань, ул. Фурманова, 3
';break;
        case 'smolensk':
          $curCity = 'г. Смоленск, Старокомендантская ул., 2
';break;
        case 'sochi':
          $curCity = 'г. Сочи, ул. Гастелло, 23А
';break;
        case 'spb':
          $curCity = 'г. Санкт-Петербург, Волхонское ш., 5, территория Южная часть производственной зоны Горелово
';break;
        case 'starii-oskol':
          $curCity = 'г. Старый Оскол, Заводская ул., 1А
';break;
        case 'stavropol':
          $curCity = 'г. Ставрополь, 2-я Промышленная ул., 33, микрорайон №18
';break;
        case 'sterlitamak':
          $curCity = 'г. Стерлитамак, Профсоюзная ул., 7
';break;
        case 'syrgyt':
          $curCity = 'г. Сургут, п. Восточный промрайон, Сосновая ул., 12 стр. 5
';break;
        case 'taganrog':
          $curCity = 'г. Таганрог, Поляковское ш., 22
';break;
        case 'tambov':
          $curCity = 'г. Тамбов, Кавалерийская ул., 13А
';break;
        case 'tolyati':
          $curCity = 'г. Тольятти, ул. Борковская, 76
';break;
        case 'tomsk':
          $curCity = 'г. Томск, Пролетарская ул., 38В, микрорайон Черемошники';break;
        case 'tumen':
          $curCity = 'г. Тюмень, ул. Коммунистическая, 47
';break;
        case 'tver':
          $curCity = 'г. Тверь, Московское ш., 18, стр. 1';break;
        case 'tyapse':
          $curCity = 'г. Туапсе, ул. Калараша, 20Г
';break;
        case 'tyla':
          $curCity = 'г. Тула, ул. Чмутова, 158В
';break;
        case 'uzhno-shalinsk':
          $curCity = 'г. Ююжно-Сахалинск, Железнодорожная ул., 170Б/1';break;
        case 'velikii-novgorod':
          $curCity = 'г. Великий Новгород, Базовый пер., 13/1, Лужский район
';break;
        case 'vladimir':
          $curCity = 'г. Владимир, ул. Куйбышева, 4
';break;
        case 'vladivostok':
          $curCity = 'г. Владивосток, Посадская ул., 20
';break;
        case 'volgograd':
          $curCity = 'г. Волгоград, ул. Землячки, 16
';break;
        case 'vologda':
          $curCity = 'г. Вологда, ул. Ильюшина, 9Б
';break;
        case 'volzskii':
          $curCity = 'г. Волжский, ул. 6-я Автодорога, 31В
';break;
        case 'voronezh':
          $curCity = 'г. Воронеж, ул. Землячки, 15
';break;
        case 'yalta':
          $curCity = 'г. Ялта, Дарсановский пер., 10
';break;
        case 'yaroslavl':
          $curCity = 'г. Ярославль, просп. Октября, 93
';break;
        case 'yfa':
          $curCity = 'г. Уфа, Трамвайная ул., 2
';break;
        case 'ylan-yde':
          $curCity = 'г. Улан-Удэ, 502-ой километр ул., 160
';break;
        case 'ylianovsk':
          $curCity = 'г. Ульяновск, ул. Ефремова, 52
';break;
        case 'yssyriisk':
          $curCity = 'г. Уссурийск, ул. Механизаторов, 8А
';break;
        case 'zelenodolsk':
          $curCity = 'г. Зеленодольск, у Новостроительная ул., 2/4
';break;
        case 'zheleznodorozhnii':
          $curCity = 'г. Железнодорожный, Саввинская ул., 14
';break;
      }
      return $curCity;
    }


    public function subdomainMetrics(){
      $googleTag = '';
      $yandexTag = '';
      $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
      $actual_link = str_replace('https://','',$actual_link);
      $curSub = explode('.', $actual_link)[0];
      switch ($curSub) {
        case 'abakan':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'aksai':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'almetevsk':
          $yandexTag = '';
          $googleTag = "<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src='https://www.googletagmanager.com/gtag/js?id=UA-127310355-4'></script>

<script>

window.dataLayer = window.dataLayer || [];

function gtag(){dataLayer.push(arguments);}

gtag('js', new Date());

gtag('config', 'UA-127310355-4');

</script>";
          break;
        case 'arhangelsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'armavir':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'artem':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'astrahan':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'balakovo':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'barnaul':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'berezniki':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'bisk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'blagoveshensk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'bor':
          $yandexTag = '<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter49618390 = new Ya.Metrika2({ id:49618390, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/tag.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks2"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/49618390" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->';
          $googleTag = '<!-- Global site tag (gtag.js) - Google Analytics -->

          <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127310355-2"></script>

          <script>

          window.dataLayer = window.dataLayer || [];

          function gtag(){dataLayer.push(arguments);}

          gtag("js", new Date());

          gtag("config", "UA-127310355-2");

          </script>';
          break;
        case 'borisoglebsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'bratsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'bryansk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'byddenovsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'cherepovets':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'chita':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'dmitrovgrad':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'dzerzhinsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'ekatrinburg':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'engels':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'habarovsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'ioshkar-ola':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'irkutsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'ivanovo':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'izhevsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'kaliningrad':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'kaluga':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'kamensk-shahtinskii':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'kazan':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'kineshma':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'kirov':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'komsomolsk-na-amyre':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'kostroma':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'krasnodar':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'krasnoyarsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'kyrsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'kyznetsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'lipetsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'magadan':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'magnitogorsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'miass':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'myrmansk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'nabereznie-chelni':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'nalchik':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'neftekamsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'nevinomissk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'nizhnevartovsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'nizhnii-novgorod':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'nizhnii-tagil':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'novochebksarsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'novocherkassk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'novokyznetsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'novorosiisk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'novosibirsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'noyabrsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'oktyabrskii':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'omsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'orel':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'orenbyrg':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'orsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'penza':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'perm':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'petropavlovsk-kamchatskii':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'petrozavodsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'podolsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'prokopievsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'pskov':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'pyatigorsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'ribinsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'rossosh':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'rostov-na-donu':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'salavat':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'saratov':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'sevastopol':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'severodvinsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'shahti':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'siktivkar':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'simferopol':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'sizran':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'smolensk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'sochi':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'spb':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'starii-oskol':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'sterlitamak':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'syrgyt':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'taganrog':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'tambov':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'tolyati':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'tomsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'tumen':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'tver':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'tyapse':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'tyla':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'uzhno-shalinsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'velikii-novgorod':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'vladimir':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'vladivostok':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'volgograd':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'vologda':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'volzskii':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'voronezh':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'yalta':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'yaroslavl':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'yfa':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'ylan-yde':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'ylianovsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'yssyriisk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'zelenodolsk':
          $yandexTag = '';
          $googleTag = '';
          break;
        case 'zheleznodorozhnii':
          $yandexTag = '';
          $googleTag = '';
          break;
      }
      return $array = [
        "yandexTag" => $yandexTag,
        "googleTag" => $googleTag,
      ];
    }


    public function concatWords(){

    }


    public function deleteRepeats($brand, $category_name){
      if ($brand != "") {
        $brand = mb_convert_case($brand, MB_CASE_LOWER, "UTF-8"); //регистр маленьких букв
        if (preg_match("/[а-я]+/i", $category_name)) {   //проверка на кириллицу
            $category_name = mb_convert_case($category_name, MB_CASE_LOWER, "UTF-8");
        }
      } 
      else{
        $category_name = mb_convert_case($category_name, MB_CASE_LOWER, "UTF-8"); //регистр маленьких букв
      }

      if (preg_match("/[а-я]+/i", $category_name)) {
          $text = $category_name . " " .  $brand;
      }
      else {
          $text = $brand . " " .  $category_name;
      }

      $text = implode(array_reverse(preg_split('//u', $text)));
      $text = preg_replace('/(\b[\pL0-9]++\b)(?=.*?\1)/siu', '', $text);
      $text = implode(array_reverse(preg_split('//u', $text)));
      return $text;
    }

    public function massageCheck($text){
      $text = str_ireplace("массажное оборудование", "массажные кресла", $text);
      $text = str_ireplace("Массажное оборудование", "Массажные кресла", $text);
      return $text;
    }

    public function priceVal($price){
      return sprintf('%g',$price);
    }


//ненужная функция вроде
    public function min_price($products){

      foreach ($products as $product) {
        if ($product.price > $min){
            $min = $product.min_price;
        }
      }
      return $min;
    }


    public function rassr_calc($price){
      return round( $price/36, 0, PHP_ROUND_HALF_UP);
    }


    public function checkCurrentNoIndex($link){
      $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
      $actual_link = str_replace('https://','',$actual_link);
      preg_match("/[^\/]+$/", $link, $matches);
      $last_word = $matches[0];
      if (strpos($actual_link, $last_word) !== false) {
        return true;
      }
      else{
        return false;
      }
    }
}
