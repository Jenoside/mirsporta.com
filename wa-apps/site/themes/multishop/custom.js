jQuery(document).ready(function($) {
    // Прокрутка страницы 
    $(window).scroll(function() {
        var goTop = $("#go-top");
        if ($(this).scrollTop() > 200) {
            goTop.fadeIn();
        } else {
            goTop.fadeOut();
        }

        if ($(window).scrollTop() > 200) {
            $('.cart-sheet, .cart-block').addClass('fixed');
        }
        else {
            $('.cart-sheet, .cart-block').removeClass('fixed');
        }
    });
    $('#go-top a').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    // Slider
    $('.bxslider').bxSlider({ auto: true/*, adaptiveHeight: true */});
    
    $(document).on("click", ".cart-sheet.fixed", function() {
        var href = $("#cart .cart-total").attr("href");
        if (href !== 'javascript:void(0)') {
            $(this).find("i.icon32").removeClass("icon32 cart-bw").addClass("icon16 loading");
            location.href = href;
        }
    });
    // Панель для мобильных браузеров
    $("#mobilebar-link").click(function() {
        $("#mobilebar").animate({
            left: '0'
        });
    });
    // Закрытие мобильной панели
    $("#mobilebar .close").click(function() {
        $("#mobilebar").animate({
            left: '-320px'
        });
    });
    // Копирование категорий в мобильную панель
    if ($("#main .category-sidebar").length) {
        $("#mobilebar .category-sidebar").html($("#main .category-sidebar").clone());
        
    }

    // Выезжающая корзина
    $(".cart-block").hover(function() {
        var cartBlock = $(this);
        if (!cartBlock.hasClass("fixed") && $(window).width() > 420) {
            var height = cartBlock.outerHeight();
            cartBlock.stop().animate({
                bottom: "-" + height + "px"
            }, 500, function() {
                $("#cart").addClass("open");
                cartBlock.addClass("open");
            });
        }
    }, function() {
        var cartBlock = $(".cart-block");
        cartBlock.stop().animate({
            bottom: 0
        }, 500, function() {
            $("#cart").removeClass("open");
            cartBlock.removeClass("open");
        });
    });

    // Иконки страниц
    $(".pages a").hover(function() {
        var i = $(this).find("img");
        var hoverIcon = i.data("hover");
        if (hoverIcon) {
            var image = i.attr('src');
            var lastindex = image.lastIndexOf("/");
            if (lastindex !== '-1') {
                var url = image.substring(0, lastindex + 1);
                i.attr('src', url + hoverIcon);
            }
        }
    }, function() {
        var i = $(this).find("img");
        var hoverIcon = i.data("hover");
        var originalIcon = i.data("original");
        if (hoverIcon) {
            var image = i.attr('src');
            var lastindex = image.lastIndexOf("/");
            if (lastindex !== '-1') {
                var url = image.substring(0, lastindex + 1);
                i.attr('src', url + originalIcon);
            }
        }
    });

    $("#mobile-menu").click(function() {
        var mobileMenu = $(".mobile-menu");
        if (mobileMenu.hasClass("hidden")) {
            mobileMenu.slideDown("700", function() {
                $(this).removeClass("hidden").removeAttr("style");
            });
        } else {
            mobileMenu.slideUp("700", function() {
                $(this).addClass("hidden").removeAttr("style");
            });
        }
    });

    $(document).on("click", ".f-collapsible", function() {
        var btn = $(this);
        var ul = btn.closest("h4").length ? btn.closest("h4").next() : btn.next();
        if (ul.hasClass("hidden")) {
            btn.removeClass("plus").addClass("minus");
            ul.slideDown("700", function() {
                $(this).removeClass("hidden");
            });
        } else {
            btn.removeClass("minus").addClass("plus");
            ul.slideUp("700", function() {
                $(this).addClass("hidden");
            });
        }
        return false;
    });
    
    // Всплывающая форма авторизации
    $(document).on("click", ".fly-login", function() {
        $.fancybox({
            content: $(".fly-forms .auth"),
            wrapCSS: 'fly-form',
            padding: 0,
            margin: 0,
            width: 725,
            autoSize: false,
            height: 'auto'
        });
        return false;
    });
    // Всплывающая форма восстановления пароля
    $(document).on("click", ".f-forgotpasswd", function() {
        $.fancybox({
            content: $(".fly-forms .forgotpasswd"),
            wrapCSS: 'fly-form',
            padding: 0,
            margin: 0,
            width: 370,
            autoSize: false,
            height: 'auto'
        });
        return false;
    });
    $(document).on("click", ".forgotpasswd-submit a", function() {
        $(".fly-login").click();
        return false;
    });

    $(document).on("focus", ".wa-error", function() {
        $(this).removeClass("wa-error");
    });
});

function escapeHtml(text) {
    return text.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
}
function defaultInputValue(input, cssClass) {
    if (!(input instanceof jQuery)) {
        input = $(input);
    }
    var defValue = input.attr('placeholder');
    var onBlur = function() {
        var v = input.val();
        if (!v || v == defValue) {
            input.val(defValue);
            input.addClass(cssClass);
        }
    };
    onBlur();
    input.blur(onBlur);
    input.focus(function() {
        if (input.hasClass(cssClass)) {
            input.removeClass(cssClass);
            input.val('');
        }
    });
}
/*Version 1.3 */
function bouncePopup(elemToFall, message) {
    $(".bounce-popup").text(message);
    var popup = $(".bounce-popup");
    var offset = elemToFall.offset();
    var w = elemToFall.outerWidth();
    var dh = popup.outerHeight()+9;
    var dw = popup.outerWidth();
    var initLeft = offset.left + ((w/2) - (dw/2));
    var finalTop = offset.top - $(window).scrollTop()-dh;
    popup.css({
            left: initLeft,
            top: offset.top - $(window).scrollTop()-dh-100,
            display: 'block',
            position: 'fixed'
        }).animate({
            left: initLeft,
            top: finalTop,
            opacity: 1
        }, 250).animate({
            top: finalTop-10
        }, 150).animate({
            top: finalTop
        }, 100, function() {
            var that = $(this);
            setTimeout(function() {
                that.hide();
            }, 1500);
        }); 
}