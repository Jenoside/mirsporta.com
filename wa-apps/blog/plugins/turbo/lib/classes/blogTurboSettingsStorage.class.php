<?php


class blogTurboSettingsStorage
{
	public function getSettings()
	{
		$app_settings_model = new waAppSettingsModel();
		$settings_json = $app_settings_model->get('blog.turbo', 'settings');
		$settings_state = json_decode($settings_json, true);
		
		if (!$settings_state)
		{
			$settings_state = array();
		}
		
		$settings_state = array_merge(array(
			'is_enabled' => false,
			'title' => '',
			'description' => '',
			'language' => 'ru',
			'number_of_posts' => '500',
			'is_enabled_pages' => false,
			'posts_per_page' => '50',
		), $settings_state);
		
		$settings = new blogTurboSettings();
		
		$settings->setIsEnabled($settings_state['is_enabled']);
		$settings->setTitle($settings_state['title']);
		$settings->setDescription($settings_state['description']);
		$settings->setLanguage($settings_state['language']);
		$settings->setNumberOfPosts($settings_state['number_of_posts']);
		$settings->setIsEnabledPages($settings_state['is_enabled_pages']);
		$settings->setPostsPerPage($settings_state['posts_per_page']);
		
		return $settings;
	}
}