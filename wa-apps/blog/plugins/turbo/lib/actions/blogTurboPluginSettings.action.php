<?php


class blogTurboPluginSettingsAction extends waViewAction
{
	public function execute()
	{
		$settings_storage = new blogTurboSettingsStorage();
		$settings = $settings_storage->getSettings();
		
		$this->view->assign('settings', $settings->toArray());
	}
}