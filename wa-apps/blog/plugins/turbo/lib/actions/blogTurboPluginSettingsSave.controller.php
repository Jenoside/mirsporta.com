<?php


class blogTurboPluginSettingsSaveController extends waJsonController
{
	public function execute()
	{
		$json_state = waRequest::post('state');
		$app_settings_model = new waAppSettingsModel();
		$app_settings_model->set('blog.turbo', 'settings', $json_state);
	}
}