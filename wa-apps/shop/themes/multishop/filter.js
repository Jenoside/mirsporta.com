jQuery(document).ready(function ($) {
    $(".slider-range").each(function () {
        var min = $(this).prev().find(".min");
        var max = $(this).prev().find(".max");
        var slider = $(this);
        slider.slider({
            range: true,
            min: parseFloat(min.attr('placeholder')),
            max: parseFloat(max.attr('placeholder')),
            step: (parseFloat(max.attr('placeholder')) - parseFloat(min.attr('placeholder'))) <= 5 ? 0.1 : 1,
            values: [parseFloat(min.val().length ? min.val() : min.attr('placeholder')),
                parseFloat(max.val().length ? max.val() : max.attr('placeholder'))],
            slide: function (event, ui) {
                var v = ui.values[0] == $(this).slider('option', 'min') ? '' : ui.values[0];
                min.val(v ? v : '');
                v = ui.values[1] == $(this).slider('option', 'max') ? '' : ui.values[1];
                max.val(v ? v : '');
            },
            stop: function (event, ui) {
                min.change();
            }
        });
        min.add(max).change(function () {
            var v_min = parseFloat(min.val()) || slider.slider('option', 'min');
            var v_max = parseFloat(max.val()) || slider.slider('option', 'max');
            if (v_max >= v_min) {
                slider.slider('option', 'values', [v_min, v_max]);
            }
        });
    });
    var requestTrack = [];
    $('.filters.ajax form input').change(function () {
        var input = $(this);
        var f = input.closest('form');
        var fields = f.serializeArray();
        var params = [];
        for (var i = 0; i < fields.length; i++) {
            if (fields[i].value !== '') {
                params.push(fields[i].name + '=' + fields[i].value);
            }
        }
        var url = '?' + params.join('&');
        $(window).lazyLoad && $(window).lazyLoad('sleep');
        $(requestTrack).each(function (i, jqXHR) {
            jqXHR.abort();
            requestTrack.splice(i, 1);
        });
        if (input.hasClass("f-price")) {
            var label = input.closest(".price-inputs");
        } else {
            var label = input.next("label");
        }
        label.find(".loading").remove();
        label.append("<i class='icon16 ig loading'></i>");

        $.ajax({
            type: 'get',
            url: url,
            cache: false,
            beforeSend: function (jqXHR) {
                requestTrack.push(jqXHR);
            },
            success: function (html) {
                label.find(".loading").remove();
                var productList = $(html).find('#product-list .product-list').children();
                if (productList.length) {
                    $(".sort-view").show();
                    $('#product-list .product-list').html(productList);
                    $(".product-view > .grey").html($(html).find(".product-view > .grey"));
                    var lpaging = $(html).find('.lazyloading-paging');
                    if (lpaging.length) {
                        $('.lazyloading-paging').html(lpaging.html());
                    } else {
                        $('.lazyloading-paging ul').html("<li class='selected'><a>1</a></li>");
                    }
                    var pages = $(html).find("#product-list > .block .menu-h");
                    
                    if (pages.length) {
                        if (!$("#product-list > .block .menu-h").length) {
                            $("#product-list").append("<div class='block'><ul class='menu-h'></ul></div>");
                        }
                        $("#product-list > .block .menu-h").html(pages.html());
                    } else {
                        $("#product-list > .block .menu-h").html('');
                    }
                    
                    if (!!(history.pushState && history.state !== undefined)) {
                        window.history.pushState({}, '', url);
                    }
                    $(window).lazyLoad && $(window).lazyLoad('reload');
                    if (typeof $.autobadgeFrontend !== 'undefined') {
                        $.autobadgeFrontend.reinit();
                    }
                } else {
                    $('#product-list .product-list').empty().append("<li>" + $(html).find('#product-list').html() + "</li>");
                    $(".sort-view").hide();
                }
            },
            error: function () {
                label.find(".loading").remove();
            }
        });
    });

    $(".filter-field").each(function (i, v) {
        var parent = $(this);
        if (!parent.hasClass("checked")) {
            parent.find("input").prop("checked", false);
        } else {
            parent.find("input").prop("checked", true);
        }
    });
    // Открытие / закрытие фильтра
    $(".filter-block b a").click(function () {
        var i = $(this).find("i");
        var id = $(this).parents(".filter-block").data("id");
        var filter = $.cookie('product_filter') || {};
        if (i.hasClass("open")) {
            i.removeClass("open darr").addClass("close rarr");
            i.closest(".filter-block").find(".filter-value").slideUp();
            delete filter[id];
        } else {
            i.removeClass("close rarr").addClass("open darr");
            i.closest(".filter-block").find(".filter-value").slideDown();
            filter[id] = id;
        }
        $.cookie('product_filter', filter, {expires: 30, path: '/'});
    });

    var filter = $.cookie('product_filter');
    if (filter) {
        $.each(filter, function (i, v) {
            $(".filter-block[data-id='" + v + "']").addClass("open").find("i.icon10").removeClass("rarr close").addClass("darr open");
        });
    }
});