jQuery(document).ready(function($) {
    var view = $.cookie('product_view');
    if (view == 'thumbs') {
        $(".view").find(".view-thumbs-bw").removeClass("view-thumbs-bw").addClass("view-thumbs");
        $(".view").find(".view-table").removeClass("view-table").addClass("view-table-bw");
        $("#product-list .product-list").addClass("thumbs").removeClass("table");
    } else if (view == 'table') {
        $(".view").find(".view-table-bw").removeClass("view-table-bw").addClass("view-table");
        $(".view").find(".view-thumbs").removeClass("view-thumbs").addClass("view-thumbs-bw");
        $("#product-list .product-list").addClass("table").removeClass("thumbs");
    }
    $(".f-view").click(function() {
        var btn = $(this);
        var i = btn.find("i");
        if (btn.data("view") == 'thumbs' && !i.hasClass("view-thumbs")) {
            i.removeClass("view-thumbs-bw").addClass("view-thumbs");
            $(".view").find(".view-table").removeClass("view-table").addClass("view-table-bw");
            $("#product-list .product-list").addClass("thumbs").removeClass("table");
            $.cookie('product_view', 'thumbs', { expires: 30, path: '/'});
        } else if (btn.data("view") == 'table' && !i.hasClass("view-table")) {
            i.removeClass("view-table-bw").addClass("view-table");
            $(".view").find(".view-thumbs").removeClass("view-thumbs").addClass("view-thumbs-bw");
            $("#product-list .product-list").addClass("table").removeClass("thumbs");
            $.cookie('product_view', 'table', { expires: 30, path: '/'});
        }
    });
});