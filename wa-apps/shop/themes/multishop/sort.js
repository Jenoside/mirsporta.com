jQuery(document).ready(function($) {
    var activeSort = $("#sorting-select option:selected");
    var id = activeSort.data("id");
    var clone = activeSort.clone();
    var sortingBlockLi = $("#sorting-block li[data-id='" + id + "']");
    if (sortingBlockLi.length) {
        if (sortingBlockLi.html().search('sort-asc') > 0) {
            activeSort.append("↑");
            clone.append("↓");
        } else {
            activeSort.append("↓");
            clone.append("↑");
        }
    }
    activeSort.hide();
    clone.prop('selected', false);
    activeSort.after(clone);
    $.each($("#sorting-block li"), function(i, v) {
        $("#sorting-select option[data-id='" + $(this).data('id') + "']").val($(this).find("a").attr('href'));
    });
    $("#sorting-select").change(function() {
        location.href = $(this).val();
    });
});