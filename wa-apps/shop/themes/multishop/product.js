jQuery(document).ready(function($) {
    // compare block
    $("a.compare-add").click(function() {
        var compare = $.cookie('shop_compare');
        if (compare) {
            compare = compare.split(',');
        } else {
            compare = [];
        }
        var i = $.inArray($(this).data('product') + '', compare);
        if (i != -1) {
            compare.splice(i, 1);
        }
        compare.push($(this).data('product'));
        var compareCount = compare.length;
        var url = $("#compare-link").attr('href').replace(/compare\/.*$/, 'compare/' + compare.join(',') + '/');
        if (compareCount > 1) {
            $("#compare-link").attr('href', url).find('span.count').html(compareCount);
        }
        $(".bottom-fixed .compare .indicator").text(parseInt(compareCount)).parent().parent().removeClass("grey").attr('href', url);
        $("#mobilebar .compare .indicator").text(parseInt(compareCount)).parent().removeClass("grey").find("a").attr('href', url);
        $.cookie('shop_compare', compare.join(','), {expires: 30, path: $.multishop.shopUrl});
        bouncePopup($(".bottom-fixed .compare-block"), "+ " + $.multishop.translate('Added to compare list'));
        $(this).hide();
        $("a.compare-remove").show();
        return false;
    });

    $("a.compare-remove").click(function() {
        var compare = $.cookie('shop_compare');
        if (compare) {
            compare = compare.split(',');
        } else {
            compare = [];
        }
        var i = $.inArray($(this).data('product') + '', compare);
        if (i != -1) {
            compare.splice(i, 1);
        }

        $(".bottom-fixed .compare .indicator").text(parseInt(compare.length));
        $("#mobilebar .compare .indicator").text(parseInt(compare.length));
        if (compare.length) {
            var url = $("#compare-link").attr('href').replace(/compare\/.*$/, 'compare/' + compare + '/');
            $.cookie('shop_compare', compare.join(','), {expires: 30, path: $.multishop.shopUrl});
            $(".bottom-fixed .compare").attr('href', url);
            $("#mobilebar .compare a").attr('href', url);
        } else {
            $(".bottom-fixed .compare").addClass("grey").attr('href', 'javascript:void(0)');
            $("#mobilebar .compare").addClass("grey").find("a").attr('href', 'javascript:void(0)');
            $.cookie('shop_compare', '', {expires: 30, path: $.multishop.shopUrl});
        }
        bouncePopup($(".bottom-fixed .compare-block"), "- " + $.multishop.translate('Removed from compare list'));
        $(this).hide();
        $("a.compare-add").show();
        return false;
    });

});

function Product(form, options) {
    this.form = $(form);
    this.add2cart = this.form.find(".add2cart");
    this.button = this.add2cart.find("input[type=submit]");
    for (var k in options) {
        this[k] = options[k];
    }
    var self = this;
    // add to cart block: services
    this.form.find(".services input[type=checkbox]").click(function() {
        var obj = $('select[name="service_variant[' + $(this).val() + ']"]');
        if (obj.length) {
            if ($(this).is(':checked')) {
                obj.removeAttr('disabled');
            } else {
                obj.attr('disabled', 'disabled');
            }
        }
        self.updatePrice();
    });

    this.form.find(".services .service-variants").on('change', function() {
        self.updatePrice();
    });

    this.form.find('.inline-select a').click(function() {
        var d = $(this).closest('.inline-select');
        d.find('a.selected').removeClass('selected');
        $(this).addClass('selected');
        d.find('.sku-feature').val($(this).data('value')).change();
        return false;
    });
    this.form.find('.inline-select a.selected').click();

    this.form.find(".skus input[type=radio]").click(function() {
        if ($(this).data('image-id')) {
            $("#product-image-" + $(this).data('image-id')).click();
        }
        if ($(this).data('disabled')) {
            self.button.attr('disabled', 'disabled');
        } else {
            self.button.removeAttr('disabled');
        }
        var sku_id = $(this).val();
        self.updateSkuServices(sku_id);
        self.updatePrice();
    });
    this.form.find(".skus input[type=radio]:checked").click();

    this.form.find(".sku-feature").change(function() { 
        var key = "";
        self.form.find(".sku-feature").each(function() {
            key += $(this).data('feature-id') + ':' + $(this).val() + ';';
        });
        var sku = self.features[key];
        if (typeof $.hideskusPlugin !== 'undefined') {
            if (!$.hideskusPlugin.start(self, key)) {
                return false;
            }
        }
        if (sku) {
            if (sku.image_id) {
                $("#product-image-" + sku.image_id).click();
            }
            self.updateSkuServices(sku.id);
            if (sku.available) {
                self.button.removeAttr('disabled');
            } else {
                self.button.attr('disabled', 'disabled');
            }
            self.add2cart.find(".price").data('price', sku.price);
            self.updatePrice(sku.price, sku.compare_price);
        } else {
            self.form.find("div.stocks div").hide();
            self.form.find(".sku-no-stock").show();
            self.button.attr('disabled', 'disabled');
            self.add2cart.find(".compare-price").hide();
            self.add2cart.find(".price").empty();
        }
    });
    this.form.find(".sku-feature:first").change();

    if (!this.form.find(".skus input:radio:checked").length) {
        this.form.find(".skus input:radio:enabled:first").attr('checked', 'checked');
    }

    this.form.submit(function() {
        var f = $(this);
        var btn = f.find("input[type='submit']");
        btn.after("<i class='icon16 ig loading'></i>");
        $.post(f.attr('action') + '?html=' + ($.multishop.ruble == 'html' ? 0 : 1), f.serialize(), function(response) {
            btn.next("i.icon16").remove();
            if (response.status == 'ok') {
                var cart_total = $("#cart .cart-total .value");
                var cart_div = f.closest('.cart');
                if ($(window).scrollTop() >= 35) {
                    cart_total.closest('#cart').addClass("fixed");
                }
                cart_total.closest('#cart').removeClass('empty');

                if (cart_div.closest('.dialog').length) {
                    var dialog = 1;
                    var productId = $("#dialog .dialog-close").data("id");
                    var productImage = $(".product-list .product-item[data-id='" + productId + "'].dialog-open").removeClass("dialog-open").find("img");
                    var clone = $('<div></div>').append(productImage.clone());
                    var origin = productImage;
                } else {
                    var dialog = 0;
                    var clone = $('<div></div>').append($('.product-page .image img').clone());
                }
                clone.appendTo($("body"));

                var origin = (typeof origin == 'undefined') ? $(".images .image") : origin;
                var cartSheet = $(".cart-block .cart-sheet");
                clone.css({
                    top: origin.offset().top,
                    left: origin.offset().left,
                    width: origin.width() + 'px',
                    height: origin.height() + 'px',
                    position: 'absolute',
                    overflow: 'hidden',
                    zIndex: 9999
                }).animate({
                    top: cartSheet.offset().top,
                    left: cartSheet.offset().left,
                    width: 0,
                    height: 0,
                    opacity: 0.5
                }, 500, function() {
                    $(this).remove();
                    cart_total.html(response.data.total);
                });

                var cartBlock = $(".cart-block");
                if (cartBlock.hasClass("fixed")) {
                    var cartSheetI = cartBlock.find("i.icon32.cart-bw");
                    cartSheetI.addClass("open");
                    setTimeout(function() {
                        cartSheetI.removeClass("open");
                    }, 3000);
                }

                $(".bottom-fixed .mobile-cart .indicator").text(response.data.count).closest('.mobile-cart').addClass('active');
                setTimeout(function() {
                    $(".bottom-fixed .mobile-cart").removeClass('active');
                }, 3000);

                var skuId = f.find("input[name='sku_id']:checked").val();
                $(".count-block .count").text(response.data.count);
                var cartBlockContent = $(".cart-block-content");
                var emptyP = cartBlockContent.find(".empty");
                if (emptyP.length) {
                    emptyP.remove();
                }
                var count = parseInt($(".add2cart input[name='quantity']").val()) || 1;
                var existProduct = cartBlockContent.find(".cart-block-row[data-id='" + response.data.item_id + "']");
                if (existProduct.length) {
                    var quantityBlock = existProduct.find(".cart-block-quantity .qty");
                    var priceBlock = existProduct.find(".price");
                    var price = parseFloat(priceBlock.text().replace(",", ".").replace(/[^\d\.]/g, "")) + parseFloat(f.find(".all-price .price").text().replace(",", ".").replace(/[^\d\.]/g, "")) * count;
                    count += parseInt(quantityBlock.val());
                    quantityBlock.val(count);
                    priceBlock.html("~" + price.toFixed(2).replace(".", ",") + " " + $("#currencies").data('currency'));
                    if (response.data.discount_numeric > 0) {
                        $(".cart-block-discount").show().find("span").html(response.data.discount);
                    } else {
                        $(".cart-block-discount").hide();
                    }
                } else {
                    var hide = false;
                    if (cartBlockContent.find(".cart-block-row").length >= $.multishop.productsPerFly) {
                        cartBlockContent.find(".show-all").show();
                        hide = true;
                    }

                    var name = f.find("li[data-id='" + skuId + "'] .s-radio-name");
                    var price = parseFloat(f.find(".all-price .price").text().replace(",", ".").replace(/[^\d\.]/g, "")) * count;
                    var html = '<div class="cart-block-row" ' + (hide ? 'style="display: none;"' : '') + ' data-id="' + response.data.item_id + '">' +
                            '<div class="cart-block-image">' +
                            (dialog ? f.find(".image").data("small") : $(".product-page").find(".image").data("small")) +
                            '</div>' +
                            '<div class="cart-block-title">' + (dialog ? '<a href="' + f.find("h4").data("url") + '">' + escapeHtml(f.find("h4").data("name")) + '</a>' : escapeHtml($(".product-info").find("h1[itemprop='name']").text())) + (name.text() ? " (" + escapeHtml(name.text()) + ")" : "") + '</div>' +
                            '<div class="cart-block-quantity">' +
                            $.multishop.translate('quantity') + ':' +
                            '<a href="javascript:void(0)" title="' + $.multishop.translate('decrease') + '" class="f-minus"><i class="icon16 ig minus"></i></a>' +
                            '<input type="text" value="' + count + '" class="qty" />' +
                            '<a href="javascript:void(0)" title="' + $.multishop.translate('increase') + '" class="f-plus"><i class="icon16 ig plus"></i></a>' +
                            '</div>' +
                            '<div class="price">~' + price.toFixed(2).replace(".", ",") + " " + $("#currencies").data('currency') + '</div>' +
                            '<a class="delete" rel="' + response.data.item_id + '" title="' + $.multishop.translate('Delete') + '" href="javascript:void(0)">' +
                            '<i class="icon16 ig close-bw"></i>' +
                            '</a>' +
                            '</div>';
                    cartBlockContent.prepend(html).show().next().show();
                    if (response.data.discount_numeric > 0) {
                        $(".cart-block-discount").show().find("span").html(response.data.discount);
                    } else {
                        $(".cart-block-discount").hide();
                    }
                    var cartTotal = $("#cart .cart-total");
                    if (cartTotal.data("url")) {
                        cartTotal.attr("href", cartTotal.data("url"));
                    }
                }
                if ($(".bottom-fixed .mobile-cart").is(":visible")) {
                    bouncePopup($(".bottom-fixed .mobile-cart"), "+ " + $.multishop.translate('Added to cart'));
                }
                if (response.data.error) {
                    alert(response.data.error);
                }
                if (cart_div.closest('.dialog').length) {
                    cart_div.closest('.dialog').hide().find('.cart').empty();
                }
            } else if (response.status == 'fail') {
                alert(response.errors);
            }
        }, "json");
        return false;
    });
}

Product.prototype.currencyFormat = function(number, no_html) {
    // Format a number with grouped thousands
    //     // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +	 bugfix by: Michael White (http://crestidg.com)

    var i, j, kw, kd, km;
    var decimals = this.currency.frac_digits;
    var dec_point = this.currency.decimal_point;
    var thousands_sep = this.currency.thousands_sep;
    
    // input sanitation & defaults
    if (isNaN(decimals = Math.abs(decimals))) {
        decimals = 2;
    }
    if (dec_point == undefined) {
        dec_point = ",";
    }
    if (thousands_sep == undefined) {
        thousands_sep = ".";
    }

    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

    if ((j = i.length) > 3) {
        j = j % 3;
    } else {
        j = 0;
    }

    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
    kd = (decimals && (number - i) ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


    var number = km + kw + kd;
    var s = no_html ? this.currency.sign : this.currency.sign_html;
    if (!this.currency.sign_position) {
        return s + this.currency.sign_delim + number;
    } else {
        return number + this.currency.sign_delim + s;
    }
};


Product.prototype.serviceVariantHtml = function(id, name, price) {
    return $('<option data-price="' + price + '" value="' + id + '"></option>').html(name + ' (+' + this.currencyFormat(price, ($.multishop.ruble == 'html' ? 0 : 1)) + ')');
};
Product.prototype.updateSkuServices = function(sku_id) {
    this.form.find("div.stocks div").hide();
    this.form.find(".sku-" + sku_id + "-stock").show();
    for (var service_id in this.services[sku_id]) {
        var v = this.services[sku_id][service_id];
        if (v === false) {
            this.form.find(".service-" + service_id).hide().find('input,select').attr('disabled', 'disabled').removeAttr('checked');
        } else {
            this.form.find(".service-" + service_id).show().find('input').removeAttr('disabled');
            if (typeof (v) == 'string') {
                this.form.find(".service-" + service_id + ' .service-price').html(this.currencyFormat(v, ($.multishop.ruble == 'html' ? 0 : 1)));
                this.form.find(".service-" + service_id + ' input').data('price', v);
            } else {
                var select = this.form.find(".service-" + service_id + ' .service-variants');
                var selected_variant_id = select.val();
                for (var variant_id in v) {
                    var obj = select.find('option[value=' + variant_id + ']');
                    if (v[variant_id] === false) {
                        obj.hide();
                        if (obj.attr('value') == selected_variant_id) {
                            selected_variant_id = false;
                        }
                    } else {
                        if (!selected_variant_id) {
                            selected_variant_id = variant_id;
                        }
                        obj.replaceWith(this.serviceVariantHtml(variant_id, v[variant_id][0], v[variant_id][1]));
                    }
                }
                this.form.find(".service-" + service_id + ' .service-variants').val(selected_variant_id);
            }
        }
    }
};

Product.prototype.updatePrice = function(price, compare_price) { 
    if (price === undefined) {
        var input_checked = this.form.find(".skus input:radio:checked");
        if (input_checked.length) {
            var price = parseFloat(input_checked.data('price'));
            var compare_price = parseFloat(input_checked.data('compare-price'));
        } else {
            var price = parseFloat(this.add2cart.find(".price").data('price'));
        }
    }
    if (compare_price) {
        if (!this.add2cart.find(".compare-price").length) {
            this.add2cart.find(".all-price").append('<div class="compare-price"></div>');
        }
        this.add2cart.find(".compare-price").html(this.currencyFormat(compare_price, ($.multishop.ruble == 'html' ? 0 : 1))).show();
    } else {
        this.add2cart.find(".compare-price").hide();
    }
    var self = this;
    this.form.find(".services input:checked").each(function() {
        var s = $(this).val();
        if (self.form.find('.service-' + s + '  .service-variants').length) {
            price += parseFloat(self.form.find('.service-' + s + '  .service-variants :selected').data('price'));
        } else {
            price += parseFloat($(this).data('price'));
        }
    });
    this.add2cart.find(".price").html(this.currencyFormat(price, ($.multishop.ruble == 'html' ? 0 : 1)));
};