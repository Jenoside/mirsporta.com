if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(what, i) {
        i = i || 0;
        var L = this.length;
        while (i < L) {
            if (this[i] === what)
                return i;
            ++i;
        }
        return -1;
    };
}
jQuery(document).ready(function($) {

    $('.bxslider-custom').bxSlider({auto: true, pause: 6000, autoHover: true});

    $(document).on('submit', '.product-list form.addtocart', function() {
        var f = $(this);
        f.attr("data-id", "");
        var input = f.find("input[type='submit']");
        if (!input.hasClass("active")) {
            input.addClass("active");
            if (f.data('url')) {
                var d = $('#dialog');
                var c = d.find('.cart');
                c.load(f.data('url'), function() {
                    input.removeClass("active");
                    input.closest(".product-item").addClass("dialog-open");
                    c.prepend('<a href="#" class="dialog-close" data-id="' + f.find("input[name='product_id']").val() + '"></a>');
                    d.show();
                    if ((c.height() > c.find('form').height())) {
                        c.css('bottom', 'auto');
                    } else {
                        c.css('bottom', '15%');
                    }
                });
                return false;
            }
            if (!f.hasClass("not-ajax")) {
                $.post(f.attr('action') + '?html=' + ($.multishop.ruble == 'html' ? '1' : '0'), f.serialize(), function(response) {
                    input.removeClass("active");
                    if (response.status == 'ok') {
                        var cart_total = $("#cart .cart-total .value");
                        if ($(window).scrollTop() >= 35) {
                            cart_total.closest('#cart').addClass("fixed");
                        }
                        cart_total.closest('#cart').removeClass('empty');
                        if ($("table.cart").length) {
                            $(".content").parent().load(location.href, function() {
                                cart_total.html(response.data.total);
                            });
                        } else {
                            if (f.parents("#dialog").length) {
                                var origin = $(".product-list .product-item[data-id='" + f.find("input[name*='product_id']") + "']");
                            } else {
                                var origin = f.closest('.product-item');
                            }
                            var block = $('<div></div>').append(origin.html()).find("img");
                            var originImg = origin.find("img:visible");
                            var cartSheet = $(".cart-block .cart-sheet");
                            block.css({
                                zIndex: 10,
                                top: origin.offset().top,
                                left: origin.offset().left,
                                width: originImg.width() + 'px',
                                height: originImg.height() + 'px',
                                position: 'absolute',
                                overflow: 'hidden'
                            }).appendTo("body").animate({
                                top: cartSheet.offset().top,
                                left: cartSheet.offset().left,
                                width: 0,
                                height: 0,
                                opacity: 0.5
                            }, 500, function() {
                                $(this).remove();
                                cart_total.html(response.data.total);
                            });
                        }
                        if (response.data.error) {
                            alert(response.data.error);
                        } else {
                            
                            $(".bottom-fixed .mobile-cart .indicator").text(response.data.count).closest('.mobile-cart').addClass('active');
                            setTimeout(function() {
                                $(".bottom-fixed .mobile-cart").removeClass('active');
                            }, 3000);
                            
                            $(".count-block .count").text(response.data.count);
                            var cartBlock = $(".cart-block");
                            if (cartBlock.hasClass("fixed")) {
                                var cartSheetI = cartBlock.find("i.icon32.cart-bw");
                                cartSheetI.addClass("open");
                                setTimeout(function() {
                                    cartSheetI.removeClass("open");
                                }, 3000);
                            }
                            var cartBlockContent = $(".cart-block-content");
                            var emptyP = cartBlockContent.find(".empty");
                            if (emptyP.length) {
                                emptyP.remove();
                            }
                            var count = parseInt($(".add2cart input[name='quantity']").val()) || 1;
                            var existProduct = cartBlockContent.find(".cart-block-row[data-id='" + response.data.item_id + "']");

                            if (existProduct.length) {
                                var quantityBlock = existProduct.find(".cart-block-quantity .qty");
                                var priceBlock = existProduct.find(".price");
                                var price = parseFloat(priceBlock.text().replace(",", ".").replace(/[^\d\.]/g, "")) + parseFloat(origin.find(".price").text().replace(",", ".").replace(/[^\d\.]/g, "")) * count;
                                count += parseInt(quantityBlock.val());
                                quantityBlock.val(count);
                                priceBlock.html("~" + price.toFixed(2).replace(".", ",") + " " + $("#currencies").data('currency'));
                                if (response.data.discount_numeric > 0) {
                                    $(".cart-block-discount").show().find("span").html(response.data.discount);
                                } else {
                                    $(".cart-block-discount").hide();
                                }
                            } else {
                                var hide = false;
                                if (cartBlockContent.find(".cart-block-row").length >= $.multishop.productsPerFly) {
                                    cartBlockContent.find(".show-all").show();
                                    hide = true;
                                }
                                var html = '<div class="cart-block-row" ' + (hide ? 'style="display: none;"' : '') + ' data-id="' + response.data.item_id + '">' +
                                        '<div class="cart-block-image">' +
                                        origin.find(".image").data("small") +
                                        '</div>' +
                                        '<div class="cart-block-title">' + origin.find("h5 a").prop('outerHTML') + '</div>' +
                                        '<div class="cart-block-quantity">' +
                                        $.multishop.translate('quantity') + ':' +
                                        '<a href="javascript:void(0)" title="' + $.multishop.translate('decrease') + '" class="f-minus"><i class="icon16 ig minus"></i></a>' +
                                        '<input type="text" value="' + count + '" class="qty" />' +
                                        '<a href="javascript:void(0)" title="' + $.multishop.translate('increase') + '" class="f-plus"><i class="icon16 ig plus"></i></a>' +
                                        '</div>' +
                                        '<div class="price">~' + origin.find(".price").html() + '</div>' +
                                        '<a class="delete" rel="' + response.data.item_id + '" title="' + $.multishop.translate('Delete') + '" href="javascript:void(0)">' +
                                        '<i class="icon16 ig close-bw"></i>' +
                                        '</a>' +
                                        '</div>';
                                cartBlockContent.prepend(html).show().next().show();
                                if (response.data.discount_numeric > 0) {
                                    $(".cart-block-discount").show().find("span").html(response.data.discount);
                                } else {
                                    $(".cart-block-discount").hide();
                                }
                                var cartTotal = $("#cart .cart-total");
                                if (cartTotal.data("url")) {
                                    cartTotal.attr("href", cartTotal.data("url"));
                                }
                            }
                            if ($(".bottom-fixed .mobile-cart").is(":visible")) {
                                bouncePopup($(".bottom-fixed .mobile-cart"), "+ " + $.multishop.translate('Added to cart'));
                            }
                        }
                    } else if (response.status == 'fail') {
                        alert(response.errors);
                    }
                }, "json");
            }
        }
        if (!f.hasClass("not-ajax")) {
            return false;
        }
    });

    $(document).on("click", ".f-quantity-minus", function() {
        var quantity = $(this).closest(".quantity").find("input[name*='quantity']");
        if (parseInt(quantity.val()) > 1 && !$(this).find("i").hasClass('loading')) {
            quantity.val(function(i, oldval) {
                return --oldval;
            }).trigger('change');
        }
    });

    $(document).on("click", ".f-quantity-plus", function() {
        if (!$(this).find("i").hasClass('loading')) {
            $(this).closest(".quantity").find("input[name*='quantity']").val(function(i, oldval) {
                return ++oldval;
            }).trigger('change');
        }
    });

    $('.dialog').on('click', 'a.dialog-close', function() {
        var productId = $(this).data("id");
        $(".product-list .product-item[data-id='" + productId + "'].dialog-open").removeClass("dialog-open");
        $(this).closest('.dialog').hide().find('.cart').empty();
        return false;
    });

    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            var productId = $(".dialog:visible").find("a.dialog-close").data("id");
            $(".product-list .product-item[data-id='" + productId + "'].dialog-open").removeClass("dialog-open");
            $(".dialog:visible").hide().find('.cart').empty();
        }
    });

    // compare block
    $(document).on("click", ".compare-catalog", function() {
        var compare = $.cookie('shop_compare');
        if (compare) {
            compare = compare.split(',');
        } else {
            compare = [];
        }
        var i = $.inArray($(this).data('product') + '', compare);
        if (i != -1) {
            compare.splice(i, 1);
        }
        if (!$(this).hasClass("active")) {
            if ($(".bottom-fixed .compare").hasClass("grey")) {
                var url = $.multishop.shopUrl + 'compare/' + $(this).data('product') + '/';
            } else {
                var href = $(".bottom-fixed .compare").attr('href');
                var url = href.substr(0, href.length - 1) + ',' + $(this).data('product') + '/';
            }
            compare.push($(this).data('product'));
            $(this).closest(".product-item").find(".compare-catalog").addClass("active").attr('title', $.multishop.translate('Remove from compare list?'));
            $(".bottom-fixed .compare .indicator").text(parseInt(compare.length)).parent().parent().removeClass("grey").attr('href', url);
            $("#mobilebar .compare .indicator").text(parseInt(compare.length)).parent().removeClass("grey").find("a").attr('href', url);
            $.cookie('shop_compare', compare.join(','), {expires: 30, path: $.multishop.shopUrl});

            bouncePopup($(".bottom-fixed .compare-block"), "+ " + $.multishop.translate('Added to compare list'));
        } else {
            if (compare.length) {
                var url = $.multishop.shopUrl + 'compare/' + compare.join(',') + '/';
                $(".bottom-fixed .compare").removeClass("grey").find(".indicator").text(parseInt(compare.length)).parent().parent().attr('href', url);
                $("#mobilebar .compare .indicator").text(parseInt(compare.length)).parent().removeClass("grey").find("a").attr('href', url);
                $.cookie('shop_compare', compare.join(','), {expires: 30, path: $.multishop.shopUrl});
            } else {
                var url = 'javascript:void(0)';
                $(".bottom-fixed .compare").addClass("grey").find(".indicator").text(parseInt(compare.length)).parent().parent().attr('href', url);
                $("#mobilebar .compare .indicator").text(parseInt(compare.length)).parent().addClass("grey").find("a").attr('href', url);
                $.cookie('shop_compare', null, {expires: 30, path: $.multishop.shopUrl});
            }
            $(this).closest(".product-item").find(".compare-catalog").removeClass("active").attr('title', $.multishop.translate('Add to compare list?'));
            bouncePopup($(".bottom-fixed .compare-block"), "- " + $.multishop.translate('Removed from compare list'));
        }
        return false;
    });
    // favourite add
    $(document).on("click", ".favourite-catalog", function() {
        var favourite = $.cookie('shop_multishop_favourite');
        if (favourite) {
            favourite = favourite.split(',');
        } else {
            favourite = [];
        }
        var i = $.inArray($(this).data('product') + '', favourite);
        if (i != -1) {
            favourite.splice(i, 1);
        }
        if ($(".product-page").length) {
            var icon = $(this);
        } else {
            var icon = $(this).closest(".product-item").find(".favourite-catalog");
        }

        if (!$(this).hasClass("active")) {
            favourite.push($(this).data('product'));
            icon.addClass("active").attr('title', $.multishop.translate('Remove from Wish list?'));
            if ($(".product-page").length) {
                $(this).find("span").text($.multishop.translate('Remove from Wish list?'));
            }
            $.cookie('shop_multishop_favourite', favourite.join(','), {expires: 30, path: $.multishop.shopUrl});
            bouncePopup($(".bottom-fixed .favourite-block"), "+ " + $.multishop.translate('Added to Wish list'));
        } else {
            if (favourite.length) {
                $.cookie('shop_multishop_favourite', favourite.join(','), {expires: 30, path: $.multishop.shopUrl});
            } else {
                $.cookie('shop_multishop_favourite', null, {expires: 30, path: $.multishop.shopUrl});
            }

            icon.removeClass("active").attr('title', $.multishop.translate('Add to Wish list?'));
            if ($(".product-page").length) {
                $(this).find("span").text($.multishop.translate('To Wish-list'));
            }
            bouncePopup($(".bottom-fixed .favourite-block"), "- " + $.multishop.translate('Removed from Wish list'));
        }
        $(".bottom-fixed .favourite-block .indicator").text(favourite.length);
        return false;
    });
    
    // Сброс просмотренных товаров
    $(".cancel-viewed").click(function() {
        $(this).after("<i class='icon16 loading'></i>");
        $.cookie('shop_multishop_viewed', null, { path: $.multishop.shopUrl });
        location.reload();
    });
    
    // Счетчик Промо
    if ($.fn.countdowntimer) {
        $('.js-promo-countdown').each(function () {
            var $this = $(this).html('');
            var id = ($this.attr('id') || 'js-promo-countdown' + ('' + Math.random()).slice(2));
            $this.attr('id', id);
            var start = $this.data('start').replace(/-/g, '/');
            var end = $this.data('end').replace(/-/g, '/');
            $this.countdowntimer({
                startDate: start,
                dateAndTime: end,
                size: 'md'
            });
        });
    }
});