$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('#scroller').fadeIn();
        } else {
            $('#scroller').fadeOut();
        }
    });
    $('#scroller').click(function () {
        $('body,html').animate({
            scrollTop : 0
        }, 400);
        return false;
    });

    // countdown
    if ($.fn.countdowntimer) {
        $('.js-promo-countdown').each(function () {
            var $this = $(this).html('');
            var id = ($this.attr('id') || 'js-promo-countdown' + ('' + Math.random()).slice(2));
            $this.attr('id', id);
            var start = $this.data('start').replace(/-/g, '/');
            var end = $this.data('end').replace(/-/g, '/');
            $this.countdowntimer({
                startDate: start,
                dateAndTime: end,
                size: 'lg'
            });
        });
    }

    $("#menu-collapsible .tree > a").after('<a href="#" class="show_cat"><i class="fa fa-angle-down"></i></a>');
    $("#menu-collapsible ul.menu-v li.selected").find("ul:first").css("display", "block");
    $("#menu-collapsible ul.menu-v li.selected").parents("ul").css("display", "block");

    $(".show_cat").click(function () {
        $(this).parent(".tree").find("ul:first").slideToggle();
        return false;
    });

    // Clear favorite
    $(".clearfavorite").click(function () {
        $.cookie('shop_favorite', '', {
            expires: 30,
            path: '/'
        });
        location.reload();
        return false;
    });

    // Clear favorite
    $(".clearview").click(function () {
        $.cookie('shop_view', '', {
            expires: 30,
            path: '/'
        });
        location.reload();
        return false;
    });

    // Сравнение товаров
    $('#compare-clear').on('click', function () {
        $.cookie('shop_compare', null, {path: '/'});
        location.href = location.href.replace(/compare\/.*/, 'compare/');
        return false;
    });
    $("#compare-all").click(function () {
        $("#compare-table tr.same").show();
        $(this).closest('ul').find('li.selected').removeClass('selected');
        $(this).parent().addClass('selected');
        return false;
    });
    $("#compare-diff").click(function () {
        $("#compare-table tr.same").hide();
        $(this).closest('ul').find('li.selected').removeClass('selected');
        $(this).parent().addClass('selected');
        return false;
    });
    $(".compare-remove").on('click', function () {
        var compare = $.cookie('shop_compare');
        if (compare) {
            compare = compare.split(',');
        } else {
            compare = [];
        }
        var i = $.inArray($(this).data('product') + '', compare);
        if (i != -1) {
            compare.splice(i, 1)
        }
        if (compare) {
            $.cookie('shop_compare', compare.join(','), {expires: 30, path: '/'});
        } else {
            $.cookie('shop_compare', null);
        }
    });
    if ($("#compare-table").width() > 1060) {
        $(".compare-scroll").mousewheel(function (event, delta) {
            this.scrollLeft -= (delta * 60);
            event.preventDefault();
        });
    }


    $(".alert .close").click(function () {
        $(this).closest(".alert").slideUp();
        return false;
    });

    $(document).on("click", ".inc_cart", function () {
        var current = $(this).closest(".select_quantity").find(".select_input_cart").val();
        if (current >= 99) {
            var current = 99;
        }
        $(this).closest(".select_quantity").find(".select_input_cart").val(parseInt(current) + 1);

        return false;
    });

    $(document).on("click", ".dec_cart", function () {
        var current = $(this).closest(".select_quantity").find(".select_input_cart").val();
        if (current != 1) {
        }
        if (current == 1) {
            var current = 2;
        }
        $(this).closest(".select_quantity").find(".select_input_cart").val(parseInt(current) - 1);
        return false;
    });


    $(".filtergroup .panel-body").click(function () {
        $(this).parent(".filtergroup").find(".slideblock").slideToggle();

        if ($(this).is(".open")) {
            $(this).removeClass("open");
            $(this).find(".icon i").removeClass("fa fa-angle-up");
            $(this).find(".icon i").addClass("fa fa-angle-down");

        } else {
            $(this).addClass("open");
            $(this).find(".icon i").removeClass("fa fa-angle-down");
            $(this).find(".icon i").addClass("fa fa-angle-up");

        }

        return false;
    });
    var f = function () {
        //product filtering
        var ajax_form_callback = function (f) {
            var fields = f.serializeArray();
            var params = [];
            for (var i = 0; i < fields.length; i++) {
                if (fields[i].value !== '') {
                    params.push(fields[i].name + '=' + fields[i].value);
                }
            }
            var url = '?' + params.join('&');
            $(window).lazyLoad && $(window).lazyLoad('sleep');

            $('#product-list').html('<img src="' + f.data('loading') + '" class="loading_icon">');
            $.get(url + '&_=_', function (html) {
                var tmp = $('<div></div>').html(html);
                $('#product-list').html(tmp.find('#product-list').html());
                if (!!(history.pushState && history.state !== undefined)) {
                    window.history.pushState({}, '', url);
                }
                $(window).lazyLoad && $(window).lazyLoad('reload');
                // Init tooltips
                $('.preview_icon').tooltipster({
                    animation: 'grow',
                    position: 'left'
                });

                $(".showtype .btn-catalog").removeClass("active");
                var cookie = $.cookie('show');
                if (!cookie || cookie == 'thumbs') {
                    $(".catalog").removeAttr('id', '');
                    $("button[data-list=thumbs]").addClass("active");
                } else if (cookie == 'list') {
                    $('.catalog').attr("id", "list");
                    $("button[data-list=list]").addClass("active");
                }

                compareProduct();
                favoriteProduct();

            });
        };

        $('.filters.ajax form input').change(function () {
            ajax_form_callback($(this).closest('form'));
        });
        $('.filters.ajax form').submit(function () {
            ajax_form_callback($(this));
            return false;
        });

        $('.filters .slider').each(function () {
            if (!$(this).find('.filter-slider').length) {
                $(this).append('<div class="filter-slider"></div>');
            } else {
                return;
            }
            var min = $(this).find('.min');
            var max = $(this).find('.max');
            var min_value = parseFloat(min.attr('placeholder'));
            var max_value = parseFloat(max.attr('placeholder'));
            var step = 1;
            var slider = $(this).find('.filter-slider');
            if (slider.data('step')) {
                step = parseFloat(slider.data('step'));
            } else {
                var diff = max_value - min_value;
                if (Math.round(min_value) != min_value || Math.round(max_value) != max_value) {
                    step = diff / 10;
                    var tmp = 0;
                    while (step < 1) {
                        step *= 10;
                        tmp += 1;
                    }
                    step = Math.pow(10, -tmp);
                    tmp = Math.round(100000 * Math.abs(Math.round(min_value) - min_value)) / 100000;
                    if (tmp && tmp < step) {
                        step = tmp;
                    }
                    tmp = Math.round(100000 * Math.abs(Math.round(max_value) - max_value)) / 100000;
                    if (tmp && tmp < step) {
                        step = tmp;
                    }
                }
            }
            slider.slider({
                range: true,
                min: parseFloat(min.attr('placeholder')),
                max: parseFloat(max.attr('placeholder')),
                step: step,
                values: [parseFloat(min.val().length ? min.val() : min.attr('placeholder')),
                    parseFloat(max.val().length ? max.val() : max.attr('placeholder'))],
                slide: function (event, ui) {
                    var v = ui.values[0] == $(this).slider('option', 'min') ? '' : ui.values[0];
                    min.val(v);
                    v = ui.values[1] == $(this).slider('option', 'max') ? '' : ui.values[1];
                    max.val(v);
                },
                stop: function (event, ui) {
                    min.change();
                }
            });
            min.add(max).change(function () {
                var v_min = min.val() === '' ? slider.slider('option', 'min') : parseFloat(min.val());
                var v_max = max.val() === '' ? slider.slider('option', 'max') : parseFloat(max.val());
                if (v_max >= v_min) {
                    slider.slider('option', 'values', [v_min, v_max]);
                }
            });
        });
    }
    f();
    $.fn.toggleChecked = function () {
        return this.each(function () {
            this.checked = this.unchecked;
        });
    };
    $(".slideall a").click(function () {
        var current = $(this);
        if ($(this).is(".open")) {
            current.removeClass("open");
            current.html(current.data("close"));
        } else {
            current.addClass("open");
            current.html(current.data("open"));
        }
        $(".slideblock").slideToggle();

        return false;
    });
    $('.clearfilter').click(function () {
        $('.checkbox').find('input').toggleChecked();
        $('.radio').find(".any").attr("checked", true);
        var value1 = parseFloat($("input[name=price_min]").attr('placeholder'));
        var value2 = parseFloat($("input[name=price_max]").attr('placeholder'));
        $('.filter-slider').slider({
            min: value1,
            max: value2,
            values: [value1, value2]

        });
        $('.rangefilter input').val('');
        $('.filterform').submit();
        return false;
    });

    $(".filter .show-filter").click(function () {
        $(this).remove();
        $(".filter .panel-default").attr('style', 'display: block !important');
        return false;
    });

    $('body').on('click', '.call_back', function () {
        var d = $('#dialog-callback');
        var c = d.find('.cart');
        d.show();
        c.prepend('<a href="#" class="dialog-close">&times;</a>');
        c.show();
        c.css('bottom', 'auto');
        $("#feedback .wa-captcha-refresh").click();
        return false;
    });

    $("#feedback").submit(function () {
        $(".fa-spinner").show();
        var data = $(this).serialize();
        $.post($(this).attr("action"), data, function (data, status) {
            if (status == 'success') {

                $(".fa-spinner").hide();
                $("#status").html(data);
            } else {
                alert('Error :(')
            }
        });

        return false;
    });


    $('body').on('click', '.preview_dialog', function () {
        var f = $(this).closest(".thumbnail").find("form");
        if (f.data('preview')) {
            var d = $('#dialog');
            var c = d.find('.cart');
            d.show();
            d.append("<div class='loading'></div>");
            c.hide();
            c.load(f.data('preview'), function () {
                d.find(".loading").remove();
                c.prepend('<a href="#" class="dialog-close">&times;</a>');
                c.show();
                c.css('bottom', 'auto');
            });
            return false;
        }
        return false;
    });

    $(".cart-form").submit(function () {
        var f = $(this);
        f.find(".addvalue").hide();
        $.post(f.attr('action'), f.serialize(), function (response) {
            if (response.status == 'ok') {
                var cart_total = $(".cart-total").fadeOut().fadeIn();
                var cart_count = $(".cart-count").fadeOut().fadeIn();
                $(this).remove();
                cart_total.html(response.data.total);
                cart_count.html(response.data.count);
            }
            if (response.status == 'fail') {
                alert(response.errors);
            }
        }, "json");

        return false;
    });

    $('body').on('click', '.addtocart', function () {
        var f = $(this).closest(".thumbnail").find("form");
        if (f.data('url')) {
            var d = $('#dialog');
            var c = d.find('.cart');
            d.show();
            d.append("<div class='loading'></div>");
            c.hide();
            c.load(f.data('url'), function () {
                d.find(".loading").remove();
                c.prepend('<a href="#" class="dialog-close">&times;</a>');
                c.show();
                c.css('bottom', 'auto');
            });
            return false;
        }
        $.post(f.attr('action') + '?html=0', f.serialize(), function (response) {
            if (response.status == "ok") {
                var cart = $('.cart-block');
                var imgtodrag = f.find(".image");
                if (imgtodrag) {
                    var origin = f.closest(".thumbnail");
                    var block = $('<div class="clone"></div>').append(origin.html());
                    var cookie = $.cookie('show');
                    if (!cookie || cookie == 'thumbs') {
                        block.find(".summary").remove();
                    }
                    block.find(".preview").remove();
                    block.css({
                        'z-index': 100500,
                        background: '#fff',
                        top: origin.offset().top,
                        left: origin.offset().left,
                        width: origin.width() + 'px',
                        height: origin.height() + 'px',
                        position: 'absolute',
                        overflow: 'hidden'
                    }).appendTo('body').css({
                        'border': '1px solid #eee',
                        'padding': '20px',
                        'text-align': 'center',
                        'background': '#fff'
                    }).animate({
                        top: cart.offset().top,
                        left: cart.offset().left,
                        width: '10px',
                        height: '10px',
                        opacity: 0.7
                    }, 700, function () {
                        $(this).remove();
                    });
                }
                cart.fadeOut('slow', function () {
                    $(this).fadeIn('slow');
                });
                $(".cart-count").html(response.data.count);
                $(".cart-total").html(response.data.total);
                f.closest(".add2cart").hide();
                f.closest(".caption").find(".added2cart").show();
            } else if (response.status == 'fail') {
                alert(response.errors);
            }
        }, "json");
        return false;
    });
    $('.dialog').on('click', 'a.dialog-close', function () {
        $(this).closest('.dialog').hide().find('.cart').empty();
        return false;
    });
    $('.dialog-callback').on('click', 'a.dialog-close', function () {
        $(this).closest('.dialog-callback').hide();
        return false;
    });

    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $(".dialog:visible").hide().find('.cart').empty();
        }
    });

    // Ajax search
    $("#search").on("keyup", function () {
        var input = $(this);
        var url_search = input.closest("form").attr("action");
        var cart = $(".cart_ajax");
        var value = $(this).val();
        if (value.length > 3) {
            var url = encodeURI(value);
            input.after("<div class='loading_ajax'></div>");
            $(".cart_ajax").css("width", input.closest(".input-group").width());
            cart.load(url_search + '?view=ajax_search&query=' + url + ' .ajax_block', function () {
                cart.show();
                cart.find(".ajax_search_link a").attr("href", url_search + '?query=' + value);
                $(".loading_ajax").remove();
                $(".hide_search").click(function () {
                    $(".cart_ajax").html("");
                    $(".loading_ajax").remove();
                    return false;
                });
            });
        } else {
            cart.hide();
            $(".cart_ajax").html("");
            $(".loading_ajax").remove();
        }
    });


    compareProduct();
    favoriteProduct();

    // View type
    $(document).on("click", ".showtype button", function () {
        $(".showtype button").removeClass("active");
        $(this).addClass("active");
        if ($(this).data("list") == 'list') {
            $('.catalog').attr("id", "list");
            $.cookie('show', 'list', {
                expires: 7,
                path: '/'
            });
        } else {
            $('.catalog').removeAttr("id");
            $.cookie('show', 'thumbs', {
                expires: 7,
                path: '/'
            });
        }
        return false;
    });

    $(document).on("click", ".product-per-page a", function () {
        var perpage = $(this).data("page");
        $.cookie('products_per_page', perpage);
        $(".filterform").submit();
        return false;
    });

    // Init tooltips
    $('.preview_icon').tooltipster({
        animation: 'grow',
        position: 'left'
    });

    // Icon catalog
    $(document).on({
        mouseenter: function () {
            $(this).find(".preview").animate({
                opacity: "show",
                right: "+10px"
            }, "fast");
        },
        mouseleave: function () {
            $(this).find(".preview").animate({
                opacity: "hide",
                right: "0px"
            }, "fast");
        }
    }, ".catalog .thumbnail");


    $(".mobile-menu").click(function () {
        $(".menu-v-background").slideToggle();
    });

    $(".logo-sm").html($(".logo a").clone());


    $('#promo').bxSlider({
        nextSelector: '#slider-product-next',
        prevSelector: '#slider-product-prev',
        nextText: '<i class="fa fa-arrow-right btn gray"></i>',
        prevText: '<i class="fa fa-arrow-left btn gray"></i>',
        moveSlides: 4,
        minSlides: 1,
        maxSlides: 4,
        slideMargin: 0,
        pager: false,
        autoHover: true,
        auto: true,
        adaptiveHeight: true,
        pause: 5000
    });

    $('#hits').bxSlider({
        nextSelector: '#slider-product-next-hits',
        prevSelector: '#slider-product-prev-hits',
        nextText: '<i class="fa fa-arrow-right btn gray"></i>',
        prevText: '<i class="fa fa-arrow-left btn gray"></i>',
        moveSlides: 4,
        minSlides: 1,
        maxSlides: 4,
        slideMargin: 0,
        pager: false,
        autoHover: true,
        auto: true,
        adaptiveHeight: true,
        pause: 5000
    });

    $('.single-slider').fractionSlider({
        'fullWidth': true,
        'controls': true,
        'pager': true,
        'responsive': true,
        'dimensions': "1000,400",
        'timeout': 5000, // default timeout before switching slides
        'speedIn': 2500, // default in - transition speed
        'speedOut': 1500,
        'increase': false,
        'pauseOnHover': false
    }).css("visibility", "visible");

    $('.main-h.menu-h').css('visibility', 'hidden');
    if ($(".main-h.menu-h").length) {
        if ($(window).width() > 767) {

        }
        updateMenu();

    }

    /** Responsive check **/
    $(window).resize(function () {
        if ($(window).width() > 500) {
            $(".menu-v-background").show();
        } else {
            $(".catalog").removeAttr("id");
        }

        $('.main-h.menu-h .drop-link').remove();
        var cnt = $(".more-menu").contents();
        $(".more-menu").replaceWith(cnt);
        $(".more").after(cnt);
        $(".more").remove();
        updateMenu();
    });

    var clone_menu = $(".main-h.menu-h").clone();
    clone_menu.removeClass("main-h");
    clone_menu.removeClass("hidden-xs-down");

    $(".mobile-menu-button li").append(clone_menu);

});

function updateMenu() {
    var container = $('.menu-h-background .col-lg-10').width();
    var full_with = 0;
    var current = 0;
    $('.main-h.menu-h > li').each(function (count) {
        full_width_count = count;
    });
    $('.main-h.menu-h > li').each(function (count) {
        full_with += $(this).width();
        if ((container - 70) < full_with) {
            current = count;
            return false;
        }
    });

    if (current > 1) {
        current = current - 1;
        $('.main-h.menu-h > li').filter(':gt(' + current + ')').wrapAll('<li class="tree more"><ul class="more-menu">');
        $(".main-h.menu-h .more").prepend("<a href='#' class='drop-link'><i class='fa fa-bars'><span class='count'>" + (full_width_count - current) + "</span></i></a>")
    }

    $('.main-h.menu-h').css('visibility', 'visible');
}

function blink(selector) {
    $(selector).fadeOut('fast', function () {
        $(this).fadeIn('fast', function () {
        });
    });
}
function favoriteProduct() {
    /** добавление в избранное */
    $(".catalog, .compare-favorite").on('click', 'a.favorite-link', function () {
        var add_text = $(this).data("add-text");
        var dec_text = $(this).data("dec-text");
        var favorite_url = $(this).data("favorite-url");
        var favorite = $.cookie('shop_favorite');
        if (favorite) {
            favorite = favorite.split(',');
        } else {
            favorite = [];
        }

        if (!$(this).hasClass('active')) {
            favorite.push($(this).data('product'));
            $.cookie('shop_favorite', favorite.join(","), {
                expires: 30,
                path: '/'
            });
            showAlert(add_text + " (" + favorite.length + ")", favorite_url);

        } else {
            var i = $.inArray($(this).data('product') + '', favorite);
            if (i != -1) {
                favorite.splice(i, 1);
                if (favorite.length > 0) {
                    $.cookie('shop_favorite', favorite.join(','), {expires: 30, path: '/'});
                } else {
                    $.cookie('shop_favorite', null, {path: '/'});
                }
            }
            showAlert(dec_text + " (" + favorite.length + ")", favorite_url);
            var type = $(this).closest(".catalog");
            if (type.data("type") == 'favorite') {
                $(this).closest(".col-lg-3").remove();
                if (favorite.length == 0) {
                    $(".thumbnail").after('<div class="alert-box notice"></div>');
                    $(".showtype-block").remove();
                    $(".clearfavorite").remove();
                }
            }
        }
        $(".favorite_count").html(favorite.length);
        $(this).toggleClass('active');
        return false;
    });
}
function compareProduct() {
    /** Сравнение продукта **/
    $(".catalog, .compare-favorite").on('click', 'a.compare-link', function () {
        var add_text = $(this).data("add-text");
        var dec_text = $(this).data("dec-text");
        var compare_url = $(this).data("compare-url");
        var compare = $.cookie('shop_compare');
        $.cookie('shop_compare', compare, {expires: 30, path: '/'});

        if (!$(this).hasClass('active')) {
            if (compare) {
                compare += ',' + $(this).data('product');
            } else {
                compare = '' + $(this).data('product');
            }
            if (compare.split(',').length > 0) {
                var url = compare_url + compare + '/';
            }
            $.cookie('shop_compare', compare, {expires: 30, path: '/'});
            showAlert(add_text + " (" + compare.split(',').length + ")", url);
        } else {
            if (compare) {
                compare = compare.split(',');
            } else {
                compare = [];
            }

            var i = $.inArray($(this).data('product') + '', compare);
            if (i != -1) {
                compare.splice(i, 1)
            }
            if (compare.length > 0) {
                $.cookie('shop_compare', compare.join(','), {expires: 30, path: '/'});
                var url = compare_url + compare.join(',') + '/';
            } else {
                $.cookie('shop_compare', null, {path: '/'});
            }
            showAlert(dec_text + " (" + compare.length + ")", url);
        }

        $(this).toggleClass('active');
        return false;
    });
}

function showAlert(message, url) {
    $(".alert-warning").remove();
    $("body").after('<div class="alert alert-warning"><a type="button" class="close"><span aria-hidden="true">&times;</span></a><a href="' + url + '">' + message + '</a></div>');
    blink(".alert-warning");
    $(".alert-warning .close").click(function () {
        $(this).closest(".alert").slideUp();
    });
}