$(function() {

    var paging = $('.lazyloading-paging');
    if (!paging.length) {
        return;
    }
    // check need to initialize lazy-loading
    var current = paging.find('li.selected');
    if (current.children('a').text() != '1') {
        return;
    }
    paging.hide();
    var win = $(window);

    // prevent previous launched lazy-loading
    win.lazyLoad('stop');

    // check need to initialize lazy-loading
    var next = current.next();
    if (next.length) {
        win.lazyLoad({
            container: '#product-list .catalog',
            load: function() {

                win.lazyLoad('sleep');
                var paging = $('.lazyloading-paging').hide();
                // determine actual current and next item for getting actual url
                var current = paging.find('li.selected');
                var next = current.next();
                var url = next.find('a').attr('href');
                if (!url) {
                    win.lazyLoad('stop');
                    return;
                }

                var product_thumbs = $('#product-list .catalog');
                var loading = paging.parent().find('.loading').parent();
                if (!loading.length) {
                    loading = $('<div><i class="icon16 loading"></i>Loading...</div>').insertBefore(paging);
                }

                loading.show();
                $.get(url, function(html) {
                    var tmp = $('<div></div>').html(html);
                    product_thumbs.append(tmp.find('#product-list .catalog').children());
                    var tmp_paging = tmp.find('.lazyloading-paging').hide();
                    paging.replaceWith(tmp_paging);
                    paging = tmp_paging;

                    // Init tooltips
                    $('.preview_icon').tooltipster({
                        animation: 'grow',
                        position: 'left'
                    });

                    // check need to stop lazy-loading
                    var current = paging.find('li.selected');
                    var next = current.next();
                    if (next.length) {
                        win.lazyLoad('wake');
                    } else {
                        win.lazyLoad('stop');
                    }

                    loading.hide();
                    tmp.remove();


                });
            }
        });
    }

});