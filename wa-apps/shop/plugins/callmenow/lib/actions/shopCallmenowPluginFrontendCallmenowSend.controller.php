<?php
class shopCallmenowPluginFrontendCallmenowSendController extends waController
{
    public function execute()
    {
  	    $theme = waRequest::post('callme_theme', '', waRequest::TYPE_STRING_TRIM);
  	    $name = waRequest::post('callme_name', '', waRequest::TYPE_STRING_TRIM);
  	    $phone = waRequest::post('callme_phone', '', waRequest::TYPE_STRING_TRIM);
		
		
		if (($name == "") || ($phone == "")) {
			exit("0");
		}
		
		$admin_mail=$this->getConfig()->getGeneralSettings('email');
		$body="Заказ звонка с сайта!  <br>Имя:".$name." <br>Телефон:".$phone." <br>Тема:".$theme."<br>";
		
		
		$mail_message = new waMailMessage('Заказ звонка с сайта. Тема:'.$theme, $body);
		//$mail_message->setFrom($admin_mail, $admin_mail);

		$mail_message->setTo($admin_mail, 'Администратору сайта');
		$mail_message->send();

		
		exit("1");
 
		
		
    }
}