<?php

return array(
    'name' => /*_wp*/('Callmenow'),
    'description' => /*_wp*/('Request a call back.'),
    'vendor' => '971447',
    'version' => '1.0',
    'frontend'    => true,
    'handlers' => array(
        'frontend_footer' => 'frontendFooter',
    ),
);

