<?php

include_once(__DIR__ . "/vendor/autoload.php");
use Symfony\Component\Finder\Finder;

class shopCompressPlugin extends shopPlugin
{
    public static function setCompressed($image)
    {
        $tmp = tempnam('/tmp', 'img');
        file_put_contents($tmp, $image);
        $type = getimagesize($tmp)[2];
        switch ($type) {
            case IMAGETYPE_JPEG:
                $iptc = iptcembed(base64_decode('HAJGAAEx'), $tmp);
                $result =  $iptc;
                break;
            case IMAGETYPE_PNG:
                $chunk = base64_decode('AAAABXRFWHRPUFQAMYF35JU=');
                $len = strlen($image);
                $result = substr($image, 0, $len - 12) . $chunk . substr($image, $len - 12, 12);
                break;
            default:
                $result = $image;
                break;
        }
        unlink($tmp);
        return $result;
    }
    
    public static function checkCompressed($file)
    {
        $info = getimagesize($file);
        $type = $info[2];
        switch ($type) {
        case IMAGETYPE_JPEG:
            getimagesize($file, $info);
            if (isset($info['APP13'])) {
                $iptc = iptcparse($info['APP13']);
                if (isset($iptc['2#070'][0]) && $iptc['2#070'][0] == 1) {
                    return true;
                }
            }
            return false;
                break;
        case IMAGETYPE_PNG:
            $str = file_get_contents($file);
            return strpos($str, base64_decode('AAAABXRFWHRPUFQAMYF35JU='));
                break;
        default:
            return true;
                break;
        }
    }

    public static function countThumbs()
    {
        $finder = new Finder();
        $finder->files()
                ->in(waConfig::get('wa_path_data') . "/public")
                ->name('/\.jpg$|\.jpeg$|\.png$/');

        return $finder->count();
    }
}
