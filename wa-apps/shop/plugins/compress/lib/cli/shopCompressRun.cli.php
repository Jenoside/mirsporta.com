<?php

class shopCompressRunCli extends waCliController
{
    private $smush = 'http://api.resmush.it/ws.php';

    public function execute()
    {
        $this->cronlog("Compress cron started.");
        $plugin = wa('shop')->getPlugin('compress');
        $path = wa()->getDataPath(null, true);
        $root_url = $plugin->getSettings('url');
        
        if (!$root_url) {
            return;
        }

        $files = waFiles::listdir($path, true);

        $this->cronlog("Compress initialized with public url " . $root_url . " and " . count($files) . " files");
        $opt = 0;
        
        foreach ($files as $file) {
            $pathname = wa()->getDataPath($file, true);
            $url = $root_url . '/shop/' . $file;

            if (!in_array(strtolower(pathinfo($file, PATHINFO_EXTENSION)), array('jpg', 'jpeg', 'png')) || shopCompressPlugin::checkCompressed($pathname)) {
                continue;
            }

            try {
                $net = new waNet();
                $o = json_decode($net->query($this->smush, array('img' => $url)));
                if (isset($o->error)) {
                    throw new waException('Cant get file from ' . $this->smush . $url);
                }
                $result = $net->query($o->dest);
            } catch (waException $e) {
                waLog::log($e->getMessage());
            }

            if (!empty($result) && @imagecreatefromstring($result)) {
                $result = shopCompressPlugin::setCompressed($result);
                $out = filesize($pathname);
                $in = waFiles::write($pathname, $result);
                $settings = $plugin->getSettings();
                $settings['in'] += $in;
                $settings['out'] += $out;
                ++$settings['count'];
                ++$opt;
                $plugin->saveSettings($settings);
            }
        }
        $this->cronlog("Compress cron ended, " . $opt . " images were optimized");
    }
    
    private function cronlog($message) {
        waLog::log($message, 'shop/plugins/compress-cron.log');
    }
}
