<?php

return array(
  'name' => 'Image compression',
  'description' => 'Progressive compression for product images',
  'img' => 'img/compress.png',
  'version' => '4.0',
  'vendor' => '1027956',
  'shop_settings' => true,
);
