<?php

return array(
    'php.gd' => array(
        'name' => 'gd',
        'description' => 'Обработка изображений',
        'strict ' => false
    ),
    'php' => array(
        'strict' => true,
        'version' => '>=5.3.9',
    ),
);
