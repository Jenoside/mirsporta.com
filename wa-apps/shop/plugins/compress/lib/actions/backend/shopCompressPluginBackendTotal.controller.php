<?php

class shopCompressPluginBackendTotalController extends waJsonController
{
    public function execute()
    {
        $cache = new waSerializeCache('compresstotal', 3600, 'shop');
        $total = $cache->get();
        if (!$total) {
            $total = shopCompressPlugin::countThumbs();
            $cache->set($total);
        }
        $this->response = $total;
    }
}
