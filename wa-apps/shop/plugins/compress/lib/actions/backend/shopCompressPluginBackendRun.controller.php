<?php

include_once(__DIR__ . "/../../vendor/autoload.php");
use Symfony\Component\Finder\Finder;

class shopCompressPluginBackendRunController extends waLongActionController
{
    private $smush = 'http://api.resmush.it/ws.php';

    public function execute()
    {
        try {
            parent::execute();
        } catch (waException $ex) {
            if ($ex->getCode() == '302') {
                echo json_encode(array('warning' => $ex->getMessage()));
            } else {
                echo json_encode(array('error' => $ex->getMessage()));
            }
        }
    }

    protected function init()
    {
        try {
            $finder = new Finder();
            $finder->files()
                    ->in(waConfig::get('wa_path_data') . "/public/shop")
                    ->name('/\.jpg$|\.jpeg$|\.png$/');
            
            foreach ($finder as $k => $file) {
                $this->data['files'][$k] = $file->getRelativePathname();
            }
            
            $this->data['count'] = count($this->data['files']);
            $this->data['current'] = 0;
            $this->data['settings'] = array();
            $this->data['done'] = false;
            $this->data['memory'] = memory_get_peak_usage();
            $this->data['memory_avg'] = memory_get_usage();
            $this->data['timestamp'] = time();
        } catch (Exception $ex) {
            sleep(5);
            $this->error($ex->getMessage()."\n".$ex->getTraceAsString());
        }
    }

    protected function info($filename = null)
    {
        $interval = 0;
        if (!empty($this->data['timestamp'])) {
            $interval = time() - $this->data['timestamp'];
        }
        $response = array(
            'time' => sprintf('%d:%02d:%02d', floor($interval / 3600), floor($interval / 60) % 60, $interval % 60),
            'current' => $this->data['current'],
            'settings' => $this->data['settings'],
            'count' => $this->data['count'],
            'processId' => $this->processId,
            'ready' => $this->isDone(),
        );
        $this->getResponse()->addHeader('Content-type', 'application/json');
        $this->getResponse()->sendHeaders();
        echo json_encode($response);
    }

    protected function infoReady($filename)
    {
        $this->info($filename);
    }

    protected function isDone()
    {
        return count($this->data['files']) < 1;
    }

    protected function step()
    {
        $result = false;
        try {
            $result = $this->stepCompress();
        } catch (Exception $ex) {
            sleep(5);
            $this->error($ex->getMessage()."\n".$ex->getTraceAsString());
        }

        return $result && !$this->isDone();
    }

    protected function stepCompress()
    {
        static $plugin;

        if (!$plugin) {
            $plugin = wa('shop')->getPlugin('compress');
        }

        $name = array_shift($this->data['files']);
        $url = wa()->getDataUrl($name, true, 'shop', true);
        $pathname = wa()->getDataPath($name, true);
        ++$this->data['current'];

        if (shopCompressPlugin::checkCompressed($pathname)) {
            return true;
        }

        try {
            $net = new waNet();
            $o = json_decode($net->query($this->smush, array('img' => $url)));
            if (isset($o->error)) {
                throw new waException('Cant get file from ' . $this->smush . $url);
            }
            $result = $net->query($o->dest);
        } catch (waException $e) {
            waLog::log($e->getMessage());
        }

        if (!empty($result) && @imagecreatefromstring($result)) {
            $result = shopCompressPlugin::setCompressed($result);
            $out = filesize($pathname);
            $in = waFiles::write($pathname, $result);
            $settings = $this->data['settings'] = $plugin->getSettings();
            $settings['in'] += $in;
            $settings['out'] += $out;
            ++$settings['count'];
            $plugin->saveSettings($settings);
        }
        
        return true;
    }

    protected function finish($filename)
    {
    }

    private function error($message)
    {
        waLog::log($message, 'shop/plugins/compress.log');
    }
}
