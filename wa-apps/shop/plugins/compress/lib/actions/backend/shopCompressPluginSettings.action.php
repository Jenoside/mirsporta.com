<?php

class shopCompressPluginSettingsAction extends waViewAction
{
    public function execute()
    {
        $plugin = wa('shop')->getPlugin('compress');
        $settings = $plugin->getSettings();

        if (!isset($settings['in'])) {
            $settings = array('in' => 0, 'out' => 0, 'count' => 0, 'quality' => 'lowest');
            $plugin->saveSettings($settings);
        }
        
        // $url = wa()->getDataUrl('products', true, 'shop', true);
        $url = wa()->getDataUrl(null, true, 'shop', true);
        $plugin->saveSettings(array('url' => $url));
        
        $this->view->assign('settings', $settings);

        $root = wa()->getDataUrl('', true, 'shop', true);
        $this->view->assign('root', $root);

        $s_root = wa()->getConfig()->getRootPath();
        $this->view->assign('s_root', $s_root);
    }
}
