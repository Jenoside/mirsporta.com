<?php

class shopChatraPluginSettingsAction extends waViewAction
{
    public function execute()
    {
        $app_config = wa()->getConfig()->getAppConfig('shop');
        $plugin_path=wa()->getAppStaticUrl('shop', true).'plugins/chatra';
        $plugin = wa()->getPlugin('chatra');
        $settings = $plugin->getSettings();
        $settings['url'] = "?plugin=chatra&action=run";
        
        $this->view->assign('app_config', $app_config->getPluginPath('chatra'));
        $this->view->assign(compact('plugin_path','settings'));
    }
}
