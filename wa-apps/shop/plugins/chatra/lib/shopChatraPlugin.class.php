<?php

class shopChatraPlugin extends shopPlugin
{
    public function frontendHead(){
        $settings = $this->getSettings();
        if(!$settings['enabled']) return;
        if(!(strpos($settings['code'],'ChatraID'))&&empty($settings['chatraID'])) return;
        if(!empty($settings['chatraID'])) {
            $chatra_code = "<script>
            ChatraID = '".$settings['chatraID']."';
            (function(d, w, c) {
                var n = d.getElementsByTagName('script')[0],
                    s = d.createElement('script');
                w[c] = w[c] || function() {
                    (w[c].q = w[c].q || []).push(arguments);
                };
                s.async = true;
                s.src = (d.location.protocol === 'https:' ? 'https:': 'http:')
                    + '//chat.chatra.io/chatra.js';
                n.parentNode.insertBefore(s, n);
            })(document, window, 'Chatra');
            </script>";
        };

        $setup= "<script>window.ChatraSetup = {
            colors: [
            '".$settings['color_text']."',
            '".$settings['color_bar']."'
            ]};</script>";
        $result='';
        if($settings['custom']) $result.= $setup;
        $result.= isset($chatra_code)?$chatra_code:$settings['code'];
        
        if($settings['send_user']) {
            $contact = wa()->getUser();
            if($contact) {
                $html = "<script>window.ChatraIntegration = {
                  name: '".$contact['name']."',
                  email: '".$contact->get('email', 'default')."'
                };</script>";
                $result.= $html;
            };
        };
        return $result;
    }
}
