<?php

return array(
    'enabled' => array(
        'title' => _wp('Plugin enabled'),
        'control_type' => waHtmlControl::CHECKBOX,
        'value' => 0,
    ),
    'code' => array(
        'title' => _wp('Code'),
        'description' => _wp('Code from Chatra site'),
        'control_type' => waHtmlControl::TEXTAREA,
        'value' => '',
    ),
    'chatraID' => array(
        'title' => _wp('ChatraID = '),
        'control_type' => waHtmlControl::INPUT,
        'value' => '',
    ),
    'send_user' => array(
        'title' => _wp('Send user info to livechat'),
        'control_type' => waHtmlControl::CHECKBOX,
        'value' => 0,
    ),
    'custom' => array(
        'title' => _wp('Custom style enabled'),
        'control_type' => waHtmlControl::CHECKBOX,
        'value' => 0,
    ),
    'color_bar' => array(
        'title' => _wp('Color bar'),
        'control_type' => waHtmlControl::INPUT,
        'value' => '#565656',
    ),
    'color_text' => array(
        'title' => _wp('Color text'),
        'control_type' => waHtmlControl::INPUT,
        'value' => '#f0f0f0',
    ),
);
