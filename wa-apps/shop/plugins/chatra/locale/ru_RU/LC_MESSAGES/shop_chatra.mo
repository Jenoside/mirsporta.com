��          �      �       0     1     L     Q  	   g  
   q  �   |  V   -  X   �     �     �              -  0   =     n     u     �     �  �   �  �   �  u     ,   �     �  G   �     #     
                                              	       Chatra - online consultant Code Code from Chatra site Color bar Color text Copy ChatraID from <a href="https://app.chatra.io/settings/general"  target="_blank">https://app.chatra.io/settings/general</a> or use full code to paste it to the field below. Copy code from https://app.chatra.io/settings/general and paste it to the field below. Create account using <a href="http://chatra.io/" target="_blank">https://chatra.io/</a>. Custom style enabled Plugin enabled Send user info to livechat ex. 1qaZ2WsX3eDc Project-Id-Version: shop/plugins/chatra
POT-Creation-Date: 2015-08-04 18:16+0300
PO-Revision-Date: 
Last-Translator:  shop/plugins/chatra
Language-Team:  shop/plugins/chatra
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=((((n%10)==1)&&((n%100)!=11))?(0):(((((n%10)>=2)&&((n%10)<=4))&&(((n%100)<10)||((n%100)>=20)))?(1):2));
X-Poedit-Language: ru_RU
X-Poedit-SourceCharset: utf-8
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: .
 Чатра - онлайн консультант Код Код с сайта Чатра Цвета панели Цвет текста Скопируйте ChatraID со страницы <a href="https://app.chatra.io/settings/general" target="_blank">https://app.chatra.io/settings/general</a> или вставьте полный код ниже. Скопируйте код со страницы https://app.chatra.io/settings/general и вставьте в поле ниже. Создайте аккаунт по адресу <a href="http://chatra.io/" target="_blank">https://chatra.io/</a>. Использовать свои цвета Плагин включен Отправлять данные о пользователе в чат к примеру 1qaZ2WsX3eDc 