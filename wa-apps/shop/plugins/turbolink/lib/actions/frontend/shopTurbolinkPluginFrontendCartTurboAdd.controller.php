<?php

class shopTurbolinkPluginFrontendCartTurboAddController extends waController {
    public function execute()
    {
        $offer_id = explode('s', waRequest::param('offer_id', 0, waRequest::TYPE_STRING));
        $settingOffer = count($offer_id) == 1 ? 'product_id' : 'sku_id';
        $url_cart_add = wa()->getRouting()->getUrl(
            'shop/frontendCart',
            array('action' => 'add'),
            true
        );

        echo "<script type='text/javascript'>
            var ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function() {
                if (ajax.readyState == 4) {
                     location.href = '".$this->cartUrl(true)."';              
                }
            }
            ajax.open('POST', '" . $url_cart_add . "', true);
            ajax.setRequestHeader('Content-type','application/x-www-form-urlencoded');
            ajax.send('".$settingOffer."=".$offer_id[count($offer_id)-1]."');
        </script><img src='".wa_url()."wa-content/img/loading16.gif'> Добавление в корзину...";
    }

    public function cartUrl($absolute = false)
    {
        $route = wa()->getRouting()->getRoute();
        $checkout_version = ifset($route, 'checkout_version', 1);
        if ($checkout_version == 2) {
            return wa()->getRouteUrl('shop/frontend/order', [], $absolute);
        }
        return wa()->getRouteUrl('shop/frontend/cart', [], $absolute);
    }
}