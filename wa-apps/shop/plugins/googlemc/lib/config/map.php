<?php
return array(
    'types'  => array(
        /**
         * fields are order sensitive and depends on DTD
         */
        'simple'       => array(
            'title'   => 'Упрощенное описание',
            'fields' => array(
                // 'available'             => true,
                'id'                    => true,
                'url'                   => true,
                'price'                 => true,
                'compare_price'         => true,
                'currencyId'            => true,
                'categoryId'            => true,
                // 'market_category'       => false,
                'picture'               => false,
                // 'store'                 => false,
                // 'pickup'                => false,
                // 'delivery'              => false,
                // 'local_delivery_cost'   => false,
                'title'                  => true,
                'brand'                => false,
                'g:color'                => false,
                'availability'            => false,
                'description'           => false,
                //'color'                    => false,
                // 'variant.color'         => false,
                // 'variant.size'          => false,
                // 'variant.size.gender'   => false,
                // 'variant.size.category' => false,
                // 'variant.size.system'   => false,
                // 'sales_notes'           => false,
                // 'manufacturer_warranty' => false,
                // 'country_of_origin'     => false,
                //'adult'                 => false,
                // 'age'                   => false,
                // 'barcode'               => false,
                'g:google_product_category' => false,
                'g:material' => false,
                'g:product_type'           => false,
                'GTIN'                     => false,
                'mpn'                      => false,
                'custom_label_0'           => false,
                'custom_label_1'           => false,
                'custom_label_2'           => false,
                'custom_label_3'           => false,
                'custom_img'                   => false,
                // 'cpa'                   => false,
                'param.*'               => false,
            ),
        ),
        'brand.model' => array(
            'title'   => 'Произвольный товар (brand.model)',
            'fields' => array(
                // 'available'             => true,
                'id'                    => true,
                'url'                   => true,
                'price'                 => true,
                'currencyId'            => true,
                'categoryId'            => true,
                'market_category'       => false,
                'picture'               => true,
                /**
                 *
                 * Ссылка на картинку соответствующего товарного предложения. Недопустимо давать ссылку на «заглушку»,
                 * т.е. на страницу, где написано «картинка отсутствует», или на логотип магазина.
                 * Максимальная длина URL — 512 символов.
                 *
                 * Для товарных предложений, относящихся к категории «Одежда и обувь», является обязательным элементом.
                 * Для всех остальных категорий – необязательный элемент.
                 **/
                'store'                 => false,
                'pickup'                => false,
                'delivery'              => false,
                'local_delivery_cost'   => false,
                'typePrefix'            => false,
                /**
                 *
                 * Группа товаров/категория.
                 *
                 * Необязательный элемент.
                 **/
                'brand'                => true,
                'availability'            => false,
                'model'                 => true,
                'description'           => false,
                'sales_notes'           => false,
                'manufacturer_warranty' => false,
                'seller_warranty'       => false,
                'country_of_origin'     => true,
                'downloadable'          => false,
                'adult'                 => false,
                'age'                   => false,
                'barcode'               => false,
                'cpa'                   => false,
                /**
                 *
                 * Элемент предназначен для управления участием товарных предложений в программе «Покупка на Маркете».
                 *
                 * Необязательный элемент.
                 **/
                'rec'                   => false,
                /**
                 *
                 * Элемент обозначает товары, рекомендуемые для покупки вместе с текущим.
                 *
                 * Необязательный элемент.
                 **/
                'expiry'                => false,
                'weight'                => false,
                'dimensions'            => true,
                'param.*'               => false,
                /**
                 *
                 * Элемент предназначен для указания характеристик товара. Для описания каждого параметра используется
                 * отдельный элемент <param>.
                 *
                 * Необязательный элемент. Элемент <offer> может содержать несколько элементов <param>.
                 */
            ),
        ),
        'book'         => array(
            'title'   => 'Книги (book)',
            'fields' => array(
                'available'           => true,
                'id'                  => true,
                'url'                 => true,
                'price'               => true,
                'currencyId'          => true,
                'categoryId'          => true,
                'market_category'     => false,
                'picture'             => false,
                'store'               => false,
                'pickup'              => false,
                'delivery'            => false,
                'local_delivery_cost' => false,
                'author'              => false,
                'title'                => true,
                'publisher'           => false,
                'series'              => false,
                'year'                => false,
                'ISBN'                => false,
                'volume'              => false,
                'part'                => false,
                'language'            => false,
                'binding'             => false,
                'page_extent'         => false,
                'table_of_contents'   => false,
                'description'         => false,
                'downloadable'        => false,
                'age'                 => false,
                'cpa'                 => false,
            ),
        ),
        'audiobook'    => array(
            'title'   => 'Аудиокниги (audiobook)',
            'fields' => array(
                'available'         => true,
                'id'                => true,
                'url'               => true,
                'price'             => true,
                'currencyId'        => true,
                'categoryId'        => true,
                'market_category'   => false,
                'picture'           => false,
                'author'            => false,
                'title'              => false,
                'publisher'         => false,
                'series'            => false,
                'year'              => false,
                'ISBN'              => false,
                'volume'            => false,
                'part'              => false,
                'language'          => false,
                'table_of_contents' => false,
                'performed_by'      => false,
                'performance_type'  => false,
                'storage'           => false,
                'format'            => false,
                'recording_length'  => false,
                'description'       => false,
                'downloadable'      => false,
                'age'               => false,
                'cpa'               => false,
            ),
        ),
        'artist.title' => array(
            'title'   => 'Музыкальная и видео продукция (artist.title)',
            'fields' => array(
                'available'       => true,
                'id'              => true,
                'url'             => true,
                'price'           => true,
                'currencyId'      => true,
                'categoryId'      => true,
                'market_category' => false,
                'picture'         => false,
                'store'           => false,
                'pickup'          => false,
                'delivery'        => false,
                'artist'          => false,
                'title'           => true,
                'year'            => false,
                'media'           => false,
                'starring'        => false,
                /**
                 * Актеры.
                 **/
                'director'        => false,
                /**
                 * Режиссер.
                 **/
                'originaltitle'    => false,
                /**
                 * Оригинальное название.
                 **/
                'country'         => false,
                /**
                 * Страна.
                 */

                'description'     => false,
                'adult'           => false,
                'age'             => false,
                'barcode'         => false,
                'GTIN'         => false,
                'cpa'             => false,
            ),
        ),
        'tour'         => array(
            'title'   => 'Туры (tour)',
            'fields' => array(
                'available'       => true,
                'id'              => true,
                'url'             => true,
                'price'           => true,
                'currencyId'      => true,
                'categoryId'      => true,
                'market_category' => false,
                'picture'         => false,
                'store'           => false,
                'pickup'          => false,
                'delivery'        => false,
                'worldRegion'     => false,
                /**
                 * Часть света.
                 **/
                'country'         => false,
                /**
                 * Страна.
                 **/
                'region'          => false,
                /**
                 * Курорт или город.
                 **/
                'days'            => true,
                'dataTour'        => false,
                'title'            => true,
                'hotel_stars'     => false,
                'room'            => false,
                'meal'            => false,
                'included'        => true,
                'transport'       => true,
                'description'     => false,
                'age'             => false,
            ),
        ),
        'event-ticket' => array(

            'title'   => 'Билеты на мероприятие (event-ticket)',
            'fields' => array(
                'available'       => true,
                'id'              => true,
                'url'             => true,
                'price'           => true,
                'currencyId'      => true,
                'categoryId'      => true,
                'market_category' => false,
                'picture'         => false,
                'store'           => false,
                'pickup'          => false,
                'delivery'        => false,
                'title'            => true,
                'place'           => true,
                'hall'            => false,
                /**
                 * Ссылка на изображение с планом зала.
                 **/
                'date'            => true,
                'is_premiere'     => false,
                /**
                 * Признак премьерности мероприятия.
                 **/
                'is_kids'         => false,
                /**
                 * Признак детского мероприятия.
                 **/
                'age'             => false,
            ),
        ),
    ),
    'fields' => array(

        /**
         * Fields order is user friendly
         */
        /*
       * 'field_id'              => array(
       * 'type'        => 'fixed|adjustable|custom',
       * 'title'        => '',
       * 'description' => '',
       * ),
       */
        'id'                    => array(
            'type'        => 'fixed',
            'title'        => 'идентификатор товарного предложения',
            'description' => '',
            'attribute'   => true,
            'source'      => 'field:id',
            'field'       => 'offer',
        ),
        'url'                   => array(
            'type'        => 'fixed',
            'title'        => 'URL — адрес страницы товара',
            'description' => '',
            'format'      => '%0.512s',
            'source'      => 'field:frontend_url',
        ),
        'price'                 => array(
            'type'        => 'fixed',
            'title'        => 'Цена',
            'description' => 'Цена товарного предложения округляеся и выводится в зависимости от настроек пользователя',
            //'format'      => '%0.2f',
            'source'      => 'field:price',
        ),
        'compare_price'                 => array(
            'type'        => 'fixed',
            'title'        => 'Зачёркнутая цена',
            'description' => 'Цена товарного предложения округляеся и выводится в зависимости от настроек пользователя',
            'format'      => '%0.2f',
            'source'      => 'field:compare_price',
        ),
        'currencyId'            => array(
            'type'        => 'fixed',
            'title'        => 'Идентификатор валюты товара',
            'description' => 'Для корректного отображения цены в национальной валюте необходимо использовать идентификатор с соответствующим значением цены',
            'values'      => array(
                'RUB',
                'USD',
                'UAH',
                'KZT',
                'BYR',
                'EUR',
            ),
            'source'      => 'field:currency',
        ),
        'categoryId'            => array(
            'type'        => 'fixed',
            'title'        => 'Идентификатор категории товара ',
            'description' => '(целое число не более 18 знаков). Товарное предложение может принадлежать только к одной категории',
            'source'      => 'field:category_id',
        ),
/*        'market_category'       => array(
            'type'        => 'fixed',
            'title'        => 'Идентификатор категории товара ',
            'description' => '',
            'source'      => 'field:market_category',
        ),*/
        'picture'               => array(
            'type'   => 'fixed',
            'title'   => 'Ссылка на изображение соответствующего товарного предложения',
            'source' => 'field:images',
        ),
        'downloadable'          => array(
            'type'        => 'fixed',
            'title'        => 'Цифровой товар',
            'description' => 'Обозначение товара, который можно скачать',
            'source'      => 'field:file_title',
        ),
        /**
         * adjustable
         */


        'brand'                => array(
            'type'        => 'adjustable',
            'title'        => 'Производитель',
            'description' => 'Если есть возможнось, то выбираем Характеристику "Бренд", если такой возможности нет, то поле "пропустить"',
        ),
        'g:color'                => array(
            'type'        => 'adjustable',
            'title'        => 'Цвет',
            'description' => 'Выбираем Характеристику "Цвет", если такой возможности нет, то поле "пропустить"',
        ),
        'availability'            => array(
            'type'        => 'adjustable',
            'title'        => 'Наличие единиц',
            'description' => 'Выбираем Основное свойство товара "в наличии"',
        ),
        'model'                 => array(
            'type'        => 'adjustable',
            'title'        => 'Модель',
            'description' => '',
        ),
        'title'                 => array(
            'type'        => 'adjustable',
            'title'        => 'Название',
            'description' => 'Название фильма или альбома',
            'source'      => 'field:title',
        ),
        'title'                  => array(
            'type'        => 'adjustable',
            'title'        => 'Название',
            'description' => 'Выбираем Основное свойство товара "Наименование"',
            'source'      => 'field:title',
        ),
        'artist'                => array(
            'type'   => 'adjustable',
            'title'   => 'Исполнитель',
            'source' => 'feature:artist',
        ),
        'author'                => array(
            'type'   => 'adjustable',
            'title'   => 'Автор произведения',
            'source' => 'feature:author'
        ),
        'days'                  => array(
            'type'   => 'adjustable',
            'title'   => 'Количество дней тура',
            'source' => '',
        ),
        'place'                 => array(
            'type'        => 'adjustable',
            'title'        => 'Место проведения',
            'description' => '',
            'format'      => '',
            'source'      => 'feature:place',
        ),
        'date'                  => array(
            'type'        => 'adjustable',
            'title'        => 'Дата и время сеанса',
            'description' => '',
            'format'      => '',
            'source'      => 'feature:date',
        ),
        'description'           => array(
            'type'        => 'adjustable',
            'title'        => 'Описание',
            'description' => 'Выбираем подходящее на Ваш взгляд описание, тоесть "описание" или "краткое описание"',
            'source'      => 'field:summary',
        ),
/*        'available'             => array(
            'type'        => 'adjustable',
            'title'        => 'Наличие',
            'description' => 'Статус доступности товара в наличии/на заказ. Выбираем "пропустить"',
            'source'      => 'field:count',
            'attribute'   => true,
            'field'       => 'offer',
            'values'      => array(*/
                /**
                 * товарное предложение на заказ. Магазин готов осуществить поставку товара на указанных условиях в течение месяца
                 * (срок может быть больше для товаров, которые всеми участниками рынка поставляются только на заказ).
                 * Те товарные предложения, на которые заказы не принимаются, не должны выгружаться в Activizm.
                 */
                // false => 'false',
                /**
                 * товарное предложение в наличии. Магазин готов сразу договариваться с покупателем о доставке товара
                 * либо товар имеется в магазине или на складе, где осуществляется выдача товара покупателям
                 **/
/*                true  => 'true',
            ),
        ),*/
/*        'variant.color'   => array(
            'type'        => 'adjustable',
            'title'        => 'Цвет',
            'description' => 'Цвет товара. Выбираем "пропустить"',
            'source'      => 'features:color',
        ),
        'variant.size'    => array(
            'type'        => 'adjustable',
            'title'        => 'Размер',
            'description' => 'Размер товара. Выбираем "пропустить"',
            'source'      => 'features:size',
        ),
        'variant.size.gender' => array(
            'type'        => 'adjustable',
            'title'        => 'Пол (для размера)',
            'description' => 'Идентификатор гендорной принадлежности. Выбираем "пропустить"',
            'attribute'   => true,
            'field'       => 'size',
            'source'      => 'features:gender',
            'values'      => array(
                'M' => 'Мужской (M)',
                'W' => 'Женский (W)',
                'J' => 'Подростковый (J)',
                'C' => 'Детский (C)',
            ),
        ),
        'variant.size.category' => array(
            'type'        => 'adjustable',
            'title'        => 'Категория (для размера)',
            'description' => 'Идентификатор товарной категории. Выбираем "пропустить"',
            'attribute'   => true,
            'field'       => 'size',
            'source'      => 'features:category',
            'values'      => array(
                'apparel'       => 'Одежда (apparel)',
                'shoes'         => 'Обувь (shoes)',
                'headgears'     => 'Головные уборы (headgears)',
                'gloves'        => 'Перчатки (gloves)',
                'skis'          => 'Лыжи и сноуборды (skis)',
                'skisticks'     => 'Лыжные палки (skisticks)',
                'skiboots'      => 'Горнолыжные ботинки (skiboots)',
                'skihelmets'    => 'Горнолыжные и сноубордические шлемы (skihelmets)',
                'boardboots'    => 'Сноубордические ботинки (boardboots)',
                'bikeframes'    => 'Велосипеды (bikeframes)',
                'boxinggloves'  => 'Боксерские перчатки (boxinggloves)',
                'boxinghelmets' => 'Боксерские шлемы (boxinghelmets)',
            ),
        ),
        'variant.size.system' => array(
            'type'        => 'adjustable',
            'title'        => 'Размерная сетка (для размера)',
            'description' => 'Идентификатор соответствия размерной сетки товара. Выбираем "пропустить"',
            'attribute'   => true,
            'field'       => 'size',
            'source'      => 'features:system',
            'values'      => array(
                'RU'  => 'RU (apparel, shoes, boardboots)',
                'INT' => 'INT (apparel, headgears, gloves, skihelmets, boxinghelmets)',
                'cm' => 'cm (shoes, gloves, skis, skisticks, skiboots, boardboots)',
                'EU' => 'EU (shoes, boardboots)',
                'US' => 'US (shoes, boardboots)',
                'UK' => 'UK (boardboots)',
                'inch' => 'inch (bikeframes)',
                'oz' => 'oz (boxinggloves)',
            ),
        ),*/
        'publisher'             => array(
            'type'   => 'adjustable',
            'title'   => 'Издательство',
            'source' => 'feature:publisher',
        ),
        'typePrefix'            => array(
            'type'   => 'adjustable',
            'title'   => 'Группа товаров/категория',
            'source' => 'field:type_id',
        ),
/*        'sales_notes'           => array(
            'type'        => 'adjustable',
            'title'        => 'Примечания',
            'description' => 'Информация о минимальной сумме заказа, минимальной партии товара или необходимости предоплаты, а также описания акций, скидок и распродаж. Выбираем "пропустить"',
            'format'      => '%50s',
        ),*/
/*        'manufacturer_warranty' => array(
            'type'        => 'adjustable',
            'title'        => 'Гарантия производителя',
            'description' => 'Информация об официальной гарантии производителя. Выбираем "пропустить"
Возможные пользовательские значения:
1) false — товар не имеет официальной гарантии;
2) true — товар имеет официальную гарантию;
3) указание срока гарантии в формате ISO 8601, например: P1Y2M10DT2H30M
4) указание срока гарантии в чисое дней;
Поддерживаются числовые данные — простое число определяет срок гарантии в днях, либо с учетом размерности характеристики типа «Время».
Остальные типы данных приводятся к значениям true/false.',
            'values'      => array(
                false => 'false',
                true  => 'true',
            ),
        ),*/
        'seller_warranty'       => array(
            'type'        => 'adjustable',
            'title'        => 'Гарантия продавца',
            'description' => 'Возможные пользовательские значения: Выбираем "пропустить"
1) false — товар не имеет гарантию продавца;
2) true — товар имеет гарантию продавца;
3) указание срока гарантии в формате ISO 8601, например: P1Y2M10DT2H30M;
4) указание срока гарантии в чисое дней;
Поддерживаются числовые данные — простое число определяет срок гарантии в днях, либо с учетом размерности характеристики типа «Время».
Остальные типы данных приводятся к значениям true/false.',
            'values'      => array(
                false => 'false',
                true  => 'true',
            ),
        ),
        'expiry'                => array(
            'type'        => 'adjustable',
            'title'        => 'Срок годности/службы',
            'description' => '
Выбираем "пропустить". Возможные пользовательские значения:
1) указание срока гарантии в формате ISO 8601, например: P1Y2M10DT2H30M
2) указание числа дней
Поддерживаются числовые данные — простое число определяет срок гарантии в днях, либо с учетом размерности характеристики типа «Время».',
            'source'      => 'feature:expiry',
        ),
/*        'country_of_origin'     => array(
            'type'        => 'adjustable',
            'title'        => 'Страна производитель',
            'description' => '',
        ),*/
/*        'adult'                 => array(
            'type'        => 'adjustable',
            'title'        => 'Товары для взрослых',
            'description' => 'Обязателен для обозначения товара, имеющего отношение к удовлетворению сексуальных потребностей либо иным образом эксплуатирующего интерес к сексу. Выбираем "пропустить"',
            'values'      => array(
                false => 'false',
                true  => 'true',
            ),
        ),*/
/*        'barcode'               => array(
            'type'        => 'adjustable',
            'title'        => 'Штрихкод',
            'description' => 'Штрихкод товара, указанный производителем. Выбираем "пропустить"',
            'source'      => 'feature:barcode'
         ), */  
        
        'GTIN'               => array(
            'type'        => 'adjustable',
            'title'        => 'GTIN',
            'description' => 'Выбираем поле GTIN если оно у Вас есть. gtin: глобальный номер товара (UPC, EAN, JAN, ISBN)',
            'source'      => 'feature:GTIN'
        ), 
        'mpn'               => array(
            'type'        => 'adjustable',
            'title'        => 'mpn',
            'description' => 'mpn: уникальный номер продукта в системе нумерации, используемой его изготовителем.',
            'source'      => 'feature:mpn'
        ),


        'g:product_type'               => array(
            'type'        => 'adjustable',
            'title'        => 'g:product_type',
            'description' => 'Позволяет по-своему классифицировать товары в фиде',

        ),

        'g:google_product_category'               => array(
            'type'        => 'adjustable',
            'title'        => 'google_product_category',
            'description' => 'Указать категорию товара по классификации Google.',
            'source'      => 'feature:g:google_product_category'
        ),
        'g:material'               => array(
            'type'        => 'adjustable',
            'title'        => 'material',
            'description' => 'Материал, из которого изготовлен товар',
            'source'      => 'feature:g:material'
        ),

        'custom_label_0'                => array(
            'type'        => 'adjustable',
            'title'        => 'Метка продовца 0',
            'description' => 'Ярлык, по которому можно группировать товары в рамках кампании. Пользователям этот атрибут не виден. Не более 100 символов',
        ),
        'custom_label_1'                => array(
            'type'        => 'adjustable',
            'title'        => 'Метка продовца 1',
            'description' => 'Ярлык, по которому можно группировать товары в рамках кампании. Пользователям этот атрибут не виден. Не более 100 символов',
        ),
        'custom_label_2'                => array(
            'type'        => 'adjustable',
            'title'        => 'Метка продовца 2',
            'description' => 'Ярлык, по которому можно группировать товары в рамках кампании. Пользователям этот атрибут не виден. Не более 100 символов',
        ),
        'custom_img'                => array(
            'type'        => 'adjustable',
            'title'        => 'свой путь до картинки',
            'description' => 'Свой абсолютный путь до картинки "https://site.ru/product.jpg"',
        ),



        /*'color'           => array(
            'type'        => 'adjustable',
            'title'        => 'Цвет',
            'description' => 'Выбираем поле цвет',
             'source'      => 'feature:color'
        ),*/
/*        'cpa'                   => array(
            'type'   => 'fixed',
            'source' => 'skip:'
        ),*/
        'series'                => array(
            'type' => 'adjustable',
            'title' => 'Серия',
        ),
        'year'                  => array(
            'type'   => 'adjustable',
            'title'   => 'Год издания',
            'format' => '%d',
            'source' => 'feature:imprint_date',
        ),
        'ISBN'                  => array(
            'type'   => 'adjustable',
            'title'   => 'Код книги',
            'source' => 'feature:isbn',
        ),
        'description.*book'     => array(
            'type' => 'adjustable',
            'title' => 'Аннотация к книге',
        ),
        'volume'                => array(
            'type' => 'adjustable',
            'title' => 'Номер тома',
        ),
        'part'                  => array(
            'type' => 'adjustable',
            'title' => 'Номер тома',
        ),
        'language'              => array(
            'type'   => 'adjustable',
            'title'   => 'Язык произведения',
            'source' => 'feature:language',
        ),
        'performed_by'          => array(
            'type' => 'adjustable',
            /**
             *  Если их несколько, перечисляются через запятую
             **/
            'title' => 'Исполнитель',
        ),
        'performance_type'      => array(
            'type' => 'adjustable',
            'title' => 'Тип аудиокниги',
        ),
        'format'                => array(
            'type' => 'adjustable',
            'title' => 'Формат аудиокниги',
        ),
        'storage'               => array(
            'type' => 'adjustable',
            'title' => 'Носитель',
        ),
        'recording_length'      => array(
            'type' => 'adjustable',
            /**
             *  задается в формате mm.ss (минуты.секунды).
             */
            'title' => 'Время звучания',
        ),
        'binding'               => array(
            'type' => 'adjustable',
            'title' => 'Переплет',
        ),
        'page_extent'           => array(
            'type' => 'adjustable',
            'title' => 'Количествово страниц в книге',
        ),
        'table_of_contents'     => array(
            'type'        => 'adjustable',
            'title'        => 'Оглавление',
            'description' => 'Выводится информация о наименованиях произведений, если это сборник рассказов или стихов. Выбираем "пропустить"',
        ),
        'weight'                => array(
            'type'        => 'adjustable',
            'title'        => 'Вес товара',
            'description' => 'Вес указывается в килограммах с учетом упаковки. Выбираем "пропустить"',
            'format'      => '%0.4f',
            'source'      => 'feature:weight',
        ),
        'dimensions'            => array(
            'type'        => 'adjustable',
            'title'        => 'Габариты товара ',
            'description' => 'габариты товара (длина, ширина, высота) в упаковке. Выбираем "пропустить"',
            'format'      => '%s',
            'source'      => '',
        ),
        'media'                 => array(
            'type'        => 'adjustable',
            'title'        => 'Носитель',
            'description' => '(CD, DVD, ...)',
        ),
        'starring'              => array(
            'type'   => 'adjustable',
            'title'   => 'Актеры',
            'source' => 'feature:starring',
        ),
        'director'              => array(
            'type'   => 'adjustable',
            'title'   => 'Режиссер',
            'source' => 'feature:director',
        ),
        'originaltitle'          => array(
            'type'   => 'adjustable',
            'title'   => 'Оригинальное название',
            'source' => '',
        ),
        'country'               => array(
            'type'   => 'adjustable',
            'title'   => 'Страна',
            'source' => '',
        ),
        'worldRegion'           => array(
            'type'   => 'adjustable',
            'title'   => 'Часть света',
            'source' => '',
        ),
        'region'                => array(
            'type'   => 'adjustable',
            'title'   => 'Курорт или город',
            'source' => '',
        ),
        'dataTour'              => array(
            'type'        => 'adjustable',
            'title'        => 'Даты заездов',
            'description' => '',
            'format'      => '',
            'source'      => '',
        ),
        'hotel_stars'           => array(
            'type'        => 'adjustable',
            'title'        => 'Звезды отеля',
            'description' => '',
            'format'      => '',
            'source'      => 'feature:hotel_stars',
        ),
        'room'                  => array(
            'type'        => 'adjustable',
            'title'        => 'Тип комнаты',
            'description' => '(SNG, DBL, ...)',
            'format'      => '',
            'source'      => 'feature:room',
        ),
        'meal'                  => array(
            'type'        => 'adjustable',
            'title'        => 'Тип питания',
            'description' => '(All, HB, ...)',
            'format'      => '',
            'source'      => 'feature:meal',
        ),
        'included'              => array(
            'type'        => 'adjustable',
            'title'        => 'Что включено в стоимость тура',
            'description' => '',
            'format'      => '',
            'source'      => 'feature:included',
        ),
        'transport'             => array(
            'type'        => 'adjustable',
            'title'        => 'Транспорт',
            'description' => '',
            'format'      => '',
            'source'      => 'feature:transport',
        ),
        'hall'                  => array(
            'type'        => 'adjustable',
            'title'        => 'Ссылка на изображение с планом зала',
            'description' => '',
            'format'      => '',
            'source'      => '',
        ),
        'is_premiere'           => array(
            'type'        => 'adjustable',
            'title'        => 'Премьера',
            'description' => 'Признак примьерности мероприятия',
            'format'      => '',
            'source'      => '',
        ),
        'is_kids'               => array(
            'type'        => 'adjustable',
            'title'        => 'Детское мероприятие',
            'description' => 'Признак детского мероприятия.',
            'format'      => '',
            'source'      => '',
        ),
/*        'age'                   => array(
            'type'        => 'adjustable',
            'title'        => 'Возрастная категория',
            'description' => '',
            'values'      => array(
                'month' => array(
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12
                ),
                'year'  => array(
                    0,
                    6,
                    12,
                    16,
                    18,
                ),
            ),
        ),*/
/*        'store'                 => array(
            'type'        => 'adjustable',
            'title'        => 'Покупка в офлайне',
            'description' => 'Возможность приобрести товар в точке продаж без предварительного заказа через интернет. Выбираем "пропустить". Остовляем поле пустое',
            'values'      => array(
                false => 'false',
                true  => 'true',
            ),
        ),*/
/*        'pickup'                => array(
            'type'        => 'adjustable',
            'title'        => 'Самовывоз',
            'description' => 'Возможность предварительно заказать товар и забрать его в точке продаж. Выбираем "пропустить". Остовляем поле пустое',
            'values'      => array(
                false => 'false',
                true  => 'true',
            ),
        ),*/
/*        'delivery'              => array(
            'type'        => 'adjustable',
            'title'        => 'Доставка',
            'description' => 'Осуществляет ли ваш магазин доставку. Выбираем "пропустить". Остовляем поле пустое и следующие 3 тоже.',
            'values'      => array(*/
                /**
                 * данный товар не может быть доставлен
                 */
                // false => 'false',
                /**
                 * товар доставляется на условиях, которые указываются в партнерском интерфейсе
                 * http:activizm.ru на странице «редактирование»
                 **/
/*                true  => 'true',
            ),

        ),*/
/*        'local_delivery_cost'   => array(
            'type'        => 'adjustable',
            'title'        => 'Стоимость доставки',
            'description' => 'Стоимость доставки данного товара в своем регионе. Выбираем "пропустить"',
        ),*/
        'param'                 => array(
            'type'        => 'adjustable',
            'title'        => '<param>',
            'description' => 'Дополнительные произвольные характеристики товара. Если тип характеристики магазина не имеет единицы измерения, но ее необходимо передать в Activizm, то можно задать название единицы измерения (параметр unit) в названии характеристики в скобках, например, «Вес (кг)»',
        ),

    )
);
