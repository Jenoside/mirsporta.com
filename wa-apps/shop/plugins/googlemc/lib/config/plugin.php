<?php

return array(
    'name'           => 'Googlemc',
    'description'    => 'Экспорт каталога товаров в feed Google Merchant Center',
    'email'          => 'joker@tjo.biz',
    'img'            => 'img/googlemc.png',
    'icons'           => array(
        16 => 'img/googlemc.png',
        200 => 'img/googlemerchantcenter.png',
    ),
    'logo'           => 'img/googlemerchantcenter.png',
    'vendor'         => '1064599',
    'version'        => '0.1.3',
    'importexport'   => 'profiles',
    'export_profile' => true,
    'frontend'       => true,
    'handlers'       => array(
        'backend_products' => 'backendProductsEvent',
    ),
);
//EOF
