<?php
return array(
    "googlemc/<hash:[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}>.xml" => array(
        'plugin' => 'googlemc',
        'module' => 'frontend',
        'action' => 'catalog',
    ),
);
