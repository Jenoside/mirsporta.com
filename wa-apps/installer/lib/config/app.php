<?php
return array(
    'name'        => 'Installer',
    'description' => 'Install new apps from the Webasyst Store',
    'icon'        => array(
        48 => 'img/installer.png',
        96 => 'img/installer96.png',
    ),
    'mobile'      => false,
    'version'     => '1.10.7',
    'critical'    => '1.10.5',
    'system'      => true,
    'vendor'      => 'webasyst',
    'csrf'        => true,
);